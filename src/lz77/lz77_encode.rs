use crate::one_to_many::OneToMany;
use crate::lz77::lz77::LZ77;
use crate::deflate::utils::lengths_to_codes;
use core::cmp;

pub const LAZY_MATCH_LEVEL_0:usize = 0;
pub const LAZY_MATCH_LEVEL_1:usize = 1;
pub const LAZY_MATCH_LEVEL_2:usize = 2;
const NICE_MATCH_LEN:usize = 64;
const MAX_MATCH_LEN:usize = 257;
const SLIDING_WINDOW:usize = 1 << 15;
const HASH_LEN:usize = 3;

pub fn input_to_lz77(input:&[u8], lazy_match:usize) -> (Vec<LZ77>, [usize; 286], [usize; 30]) {
    debug_assert!(lazy_match < HASH_LEN);
    let mut output = Vec::new();
    let mut hash_chain = OneToMany::new();
    let mut input_pointer = 0;
    let mut literal_length_weights = [0usize; 286];
    let mut distance_weights = [0usize; 30];
    literal_length_weights[256] += 1;
    let hash_chain_check_len = if input.len() > HASH_LEN + 1 {input.len() - HASH_LEN + 1} else {0};

    while input_pointer < hash_chain_check_len {
        let (mut length, mut distance) = check_duplicates(&mut hash_chain, &input, input_pointer, HASH_LEN);
        if length > 0 {
            // println!("match(0) <{},{}>", length, distance);
            let mut lazy_match_level = LAZY_MATCH_LEVEL_0;
            let mut next = 1usize;
            while lazy_match_level < lazy_match && input_pointer + next + length + 1 < input.len() {
                let (length_next, distance_next)
                    = check_duplicates(&mut hash_chain, &input, input_pointer + next, length + 1);
                if length_next > length {
                    // println!("match({}) <{},{}>", 1 - lazy_match_level, length_next, distance_next);
                    save_literal(input[input_pointer], &mut input_pointer, &mut output, &mut literal_length_weights);
                    length = length_next;
                    distance = distance_next;
                }
                else {
                    next += 1;
                }
                lazy_match_level += 1;
            }
            save_length_distance(length, distance, &mut input_pointer, &mut output,
                                 &mut hash_chain, &input,
                                 &mut literal_length_weights, &mut distance_weights);
        }
        else {
            save_literal(input[input_pointer], &mut input_pointer, &mut output, &mut literal_length_weights);
        }
    }
    while input_pointer < input.len(){
        save_literal(input[input_pointer], &mut input_pointer, &mut output, &mut literal_length_weights);
    }
    (output, literal_length_weights, distance_weights)
}

fn check_duplicates<'a>(hash_chain:&mut OneToMany<&'a [u8], usize>, input:&'a [u8], input_pointer:usize,
                        minimum_match:usize) -> (usize, usize)
{
    let pattern = &input[input_pointer .. input_pointer + HASH_LEN];
    let result = match hash_chain.get_mut(pattern) {
        Some(points) => get_length_and_point(points, input, input_pointer, minimum_match),
        None => {
            (0 , 0)
        },
    };
    hash_chain.add(pattern, input_pointer);
    result
}

fn get_length_and_point(points:&[usize], input:&[u8], input_pointer:usize, mut minimum_match:usize) -> (usize, usize) {
    let mut max_match = (0usize , 0usize);
    for i in (0 .. points.len()).rev() {
        let point = points[i];
        let distance = input_pointer - point;
        if distance > SLIDING_WINDOW {
            break;
        }
        let length = point_match_size(input, point, input_pointer, minimum_match);
        // println!("point {} len {} points {}", point, length, points.len());
        if max_match.0 < length {
            max_match = (length, distance);
            minimum_match = length;
            if length >= NICE_MATCH_LEN {
                break;
            }
        }
    }
    max_match
}

fn point_match_size(input:&[u8], mut point:usize, mut input_pointer:usize, minimum_match:usize) -> usize{
    if input[point .. point + minimum_match] != input[input_pointer .. input_pointer + minimum_match] {
        return 0;
    }
    point += minimum_match;
    input_pointer += minimum_match;
    let mut size = minimum_match;
    while input_pointer < input.len() && input[point] == input[input_pointer] && size < MAX_MATCH_LEN{
        point += 1;
        input_pointer += 1;
        size += 1;
    }
    size
}

fn save_literal(literal:u8, input_pointer:&mut usize, output:&mut Vec<LZ77>, literal_length_weights:&mut [usize; 286]){
    output.push(LZ77::Lit(literal));
    literal_length_weights[literal as usize] += 1;
    *input_pointer += 1;
}

fn save_length_distance<'a>(length:usize,  distance:usize, input_pointer:&mut usize, output:&mut Vec<LZ77>,
    hash_chain:&mut OneToMany<&'a [u8], usize>, input:&'a [u8],
    literal_length_weights:&mut [usize; 286], distance_weights:&mut [usize; 30])
{
    let literal_length_code_with_extra_bits = lengths_to_codes::length_to_code(length);
    let distance_code_with_extra_bits = lengths_to_codes::distance_to_code(distance);
    literal_length_weights[literal_length_code_with_extra_bits.code as usize] += 1;
    distance_weights[distance_code_with_extra_bits.code as usize] += 1;
    output.push(LZ77::Match(literal_length_code_with_extra_bits, distance_code_with_extra_bits));

    for i in *input_pointer .. cmp::min(*input_pointer + length, input.len() - HASH_LEN) {
        let pattern = &input[i .. i + HASH_LEN];
        hash_chain.add(pattern, i);
    }

    *input_pointer += length;
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::deflate::utils::lengths_to_codes::{length_to_code, distance_to_code};


    #[test]
    fn test_input_to_lz77() {
        let input = &[1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2];
        let (output, _, _) = input_to_lz77(input, LAZY_MATCH_LEVEL_0);
        // println!("{:?}", &output);
        assert_eq!(&output as &[LZ77], &[LZ77::Lit(1), LZ77::Lit(1), LZ77::Lit(1), LZ77::Lit(2), LZ77::Lit(2), LZ77::Lit(2),
            LZ77::Match(length_to_code(3), distance_to_code(6)),
            LZ77::Match(length_to_code(4), distance_to_code(8))] as &[LZ77]);
        let (output, _, _) = input_to_lz77(input, LAZY_MATCH_LEVEL_1);
        // println!("{:?}", &output);
        assert_eq!(&output[6..] as &[LZ77], &[LZ77::Lit(1), LZ77::Match(length_to_code(4), distance_to_code(1)),
            LZ77::Lit(2), LZ77::Lit(2)] as &[LZ77]);
        //closer first
        let input = &[1, 2, 2, 1, 2, 2, 3, 1, 2, 2];
        let (output, _, _) = input_to_lz77(input, LAZY_MATCH_LEVEL_0);
        // println!("{:?}", &output);
        assert_eq!(&output[3..] as &[LZ77], &[LZ77::Match(length_to_code(3), distance_to_code(3)),
            LZ77::Lit(3), LZ77::Match(length_to_code(3), distance_to_code(4))] as &[LZ77]);
        //longer prefers closer
        let input = &[1, 2, 2, 1, 2, 2, 3, 1, 2, 2, 1, 2];
        let (output, _, _) = input_to_lz77(input, LAZY_MATCH_LEVEL_0);
        // println!("{:?}", &output);
        assert_eq!(&output[3..] as &[LZ77], &[LZ77::Match(length_to_code(3), distance_to_code(3)),
            LZ77::Lit(3), LZ77::Match(length_to_code(5), distance_to_code(7))] as &[LZ77]);
        //lazy matching
        let input = &[1, 2, 2, 2, 2, 1, 2, 2, 1, 2];
        let (output, _, _) = input_to_lz77(input, LAZY_MATCH_LEVEL_1);
        // println!("{:?}", &output);
        assert_eq!(&output as &[LZ77], &[LZ77::Lit(1), LZ77::Lit(2),
            LZ77::Match(length_to_code(3), distance_to_code(1)),
            LZ77::Match(length_to_code(3), distance_to_code(5)),
            LZ77::Lit(1), LZ77::Lit(2)] as &[LZ77]);
    }
}