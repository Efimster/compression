use std::fmt::{Formatter, Display};
use core::fmt;
use crate::deflate::structs::code_with_extra_bits::CodeWithExtraBits;
use crate::deflate::utils::lengths_to_codes::{code_to_length, code_to_distance};

#[derive(PartialEq)]
pub enum LZ77 {
    Lit(u8),
    Match(CodeWithExtraBits, CodeWithExtraBits),
}

impl fmt::Display for LZ77 {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            LZ77::Lit(token) => write!(f, "{}", token),
            LZ77::Match(length, distance)
                => write!(f, "<{},{}>", code_to_length(length), code_to_distance(distance))
        }
    }
}

impl fmt::Debug for LZ77 {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}