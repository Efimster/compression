use std::io::{Read, Write};
use crate::zlib::zlib_error::{ZlibError, ZlibErrorKind};
use crate::deflate::decode::deflate_decode;
use crc::adler32::adler32;
use crate::deflate::encode::deflate_encode;

pub fn zlib_decode<R:Read>(input:&mut R) -> Result<Vec<u8>, ZlibError>{
    read_header(input)?;
    let mut output = vec![];
    deflate_decode(input, &mut output)?;
    let mut buf = [0u8; 4];
    input.read_exact(&mut buf)?;
    let adler_check = u32::from_be_bytes(buf);
    let adler = adler32(&output);
    if adler != adler_check {
        return Err(ZlibErrorKind::AdlerCheckFailed.into());
    }
    Ok(output)
}

fn read_header<R:Read>(input:&mut R) -> Result<(), ZlibError>{
    let mut buf = [0u8; 2];
    input.read_exact(&mut buf)?;
    debug_assert_eq!(buf[0] & 0xf, 8);
    debug_assert!((buf[0] >> 4) < 8);
    debug_assert_eq!(u16::from_be_bytes(buf) % 31, 0);
    if (buf[1] & 0b100000) > 0 {
        return Err(ZlibErrorKind::PresetDictionaryUnsupported.into());
    }
    Ok(())
}

pub fn zlib_encode<W:Write>(input:&[u8], writer:&mut W) -> Result<(), ZlibError> {
    let header = [0x78, 1];
    writer.write(&header)?;
    deflate_encode(input, writer)?;
    writer.write(&adler32(&input).to_be_bytes())?;
    Ok(())
}
