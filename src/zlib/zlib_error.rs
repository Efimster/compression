use crate::deflate::enums::deflate_error::DeflateError;
use core::fmt;
use std::{error::Error, io};

#[derive(Debug, Clone)]
pub enum ZlibErrorKind {
    AdlerCheckFailed,
    PresetDictionaryUnsupported,
    Inner
}

#[derive(Debug)]
pub struct ZlibError {
    pub source:Option<Box<dyn Error>>,
    pub kind:ZlibErrorKind,
}

impl ZlibError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        ZlibError {
            source: Some(source),
            kind: ZlibErrorKind::Inner
        }
    }

    pub fn new_kind(kind:ZlibErrorKind)->Self{
        ZlibError {
            source: None,
            kind
        }
    }
}

impl Error for ZlibError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl ZlibErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:ZlibError = self.into();
        error.into()
    }
}

impl fmt::Display for ZlibErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ZlibErrorKind::AdlerCheckFailed => write!(f, "Adler check failed"),
            ZlibErrorKind::PresetDictionaryUnsupported => write!(f, "unsupported preset dictionary"),
            ZlibErrorKind::Inner =>  write!(f, "error in ZlibError::source"),
        }
    }
}

impl fmt::Display for ZlibError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<ZlibErrorKind> for ZlibError {
    fn from(kind: ZlibErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for ZlibError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl From<io::Error> for ZlibError {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<DeflateError> for ZlibError {
    fn from(error: DeflateError) -> Self {
        Self::new_inner(error.into())
    }
}