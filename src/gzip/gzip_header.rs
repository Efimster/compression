use std::io::{Read, Write};
use crate::gzip::gzip_error::{GZipError, GZipErrorKind};
use date::date_time::DateTime;
use std::convert::TryInto;
use crate::gzip::gzip_util;
use core::fmt;
use std::fmt::{Formatter, Display};

const ID1_ID2:&[u8; 2] = &[0x1f, 0x8b];

pub struct GZipHeader {
    pub modification_time: Option<DateTime>,
    pub file_name:String,
    pub file_comment:String,
}

impl  GZipHeader {
    pub fn new<T1:ToString, T2:ToString>(file_name:T1, file_comment:T2, modification_time:Option<DateTime>) -> Self {
        GZipHeader {
            file_name:file_name.to_string(),
            file_comment:file_comment.to_string(),
            modification_time
        }
    }

    pub fn decode<R:Read>(input:&mut R) -> Result<GZipHeader, GZipError>{
        let mut buf = [0u8; 10];
        input.read_exact(&mut buf)?;
        if &buf[ .. 2] != ID1_ID2 {
            return Err(GZipErrorKind::Parse.into());
        }
        debug_assert_eq!(buf[2], 8);//Deflate, the only gzip compression method
        let seconds_from_unix_epoch = u32::from_le_bytes((&buf[4 .. 8]).try_into()?);
        let modification_time = if seconds_from_unix_epoch > 0 {
            Some(DateTime::from_seconds_since_unix_epoch(seconds_from_unix_epoch as u64))
        }
        else {
            None
        };
        let bit_flag = buf[3];
        debug_assert_eq!(bit_flag & 0xe0, 0);//reserved flags should be zero
        if gzip_util::flag_extra(bit_flag) {
            input.read_exact(&mut buf[..2])?;
            let extra_field_len = u16::from_le_bytes((&buf[ .. 2]).try_into()?) as usize;
            let mut buf = vec![0u8; extra_field_len];
            input.read_exact(&mut buf)?;
        }
        let file_name = if gzip_util::flag_file_name(bit_flag) {
            gzip_util::read_zero_terminated_string(input)?
        }
        else {
            String::new()
        };

        let file_comment = if gzip_util::flag_file_comment(bit_flag) {
            gzip_util::read_zero_terminated_string(input)?
        }
        else {
            String::new()
        };
        if gzip_util::flag_crc16(bit_flag){
            let _crc16 = u16::from_le_bytes((&buf[ .. 2]).try_into()?) as usize;
        }
        let header = GZipHeader {
            modification_time,
            file_name,
            file_comment
        };
        Ok(header)
    }

    pub fn encode<W:Write>(&self, output:&mut W) -> Result<(), GZipError>{
        output.write(ID1_ID2)?;
        output.write(&[8])?;//Deflate
        let mut bit_flag = if self.file_name.len() > 0 {0b1000u8} else {0};
        if self.file_comment.len() > 0 {
            bit_flag |= 0b10000;
        }
        output.write(&[bit_flag])?;
        let seconds = match self.modification_time {
            Some(date_time) => date_time.to_seconds_from_unix_epoch() as u32,
            None => 0,
        };
        output.write(&seconds.to_le_bytes())?;
        output.write(&[4])?;//XFL = 4 - compressor used fastest algorithm
        output.write(&[0xff])?;//OS = 255 - unknown
        if self.file_name.len() > 0 {
            gzip_util::write_zero_terminated_string(output, &self.file_name)?;
        }
        if self.file_comment.len() > 0 {
            gzip_util::write_zero_terminated_string(output, &self.file_comment)?;
        }
        Ok(())
    }
}

impl fmt::Display for GZipHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let file_name = if self.file_name.len() > 0 {&self.file_name} else {"__NO_FILE_NAME__"};
        let modification_time = match self.modification_time {
            Some(date_time) => format!(" MDate {}", date_time),
            None => String::new(),
        };
        write!(f, "{}{} {} ", file_name, modification_time, self.file_comment)
    }
}

impl fmt::Debug for GZipHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}
