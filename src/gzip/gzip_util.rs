use std::io::{Read, Write};
use crate::gzip::gzip_error::GZipError;

#[inline]
pub fn flag_text(bit_flag:u8) -> bool {
    bit_flag & 0b01 > 0
}
#[inline]
pub fn flag_crc16(bit_flag:u8) -> bool {
    bit_flag & 0b10 > 0
}
#[inline]
pub fn flag_extra(bit_flag:u8) -> bool {
    bit_flag & 0b100 > 0
}
#[inline]
pub fn flag_file_name(bit_flag:u8) -> bool {
    bit_flag & 0b1000 > 0
}
#[inline]
pub fn flag_file_comment(bit_flag:u8) -> bool {
    bit_flag & 0b10000 > 0
}

pub fn latin1_to_string(s: &[u8]) -> String {
    s.iter().map(|&c| c as char).collect()
}

pub fn read_zero_terminated_string<R:Read>(reader:&mut R) -> Result<String, GZipError> {
    let mut result = vec![];
    let mut buf = [0u8; 1];
    loop {
        reader.read_exact(&mut buf)?;
        if buf[0] == 0 {
            break;
        }
        result.push(buf[0]);
    }
    Ok(latin1_to_string(&result))
}

pub fn write_zero_terminated_string<W:Write>(writer:&mut W, string:&str) -> Result<(), GZipError>{
    writer.write(string.as_bytes())?;
    writer.write(&[0])?;//zero-terminated
    Ok(())
}