use std::io::{Read, Write};
use crate::gzip::gzip_error::{GZipError, GZipErrorKind};
use crate::gzip::gzip_header::GZipHeader;
use crate::deflate::decode::deflate_decode;
use std::convert::TryInto;
use crate::deflate::encode::deflate_encode;
use date::date_time::DateTime;
use crc::crc32::crc32;

pub fn gzip_decode<R:Read>(input:&mut R) -> Result<(GZipHeader, Vec<u8>), GZipError>{
    let header = GZipHeader::decode(input)?;
    let output = gzip_decode_compressed_data(input)?;
    Ok((header, output))
}

pub fn gzip_ls<R:Read>(input:&mut R) -> Result<GZipHeader, GZipError>{
    GZipHeader::decode(input)
}

pub fn gzip_decode_compressed_data<R:Read>(input:&mut R) -> Result<Vec<u8>, GZipError>{
    let mut output = vec![];
    deflate_decode(input, &mut output)?;
    let mut buf = [0u8; 8];
    input.read_exact(&mut buf)?;
    let crc_check = u32::from_le_bytes((&buf[ .. 4]).try_into()?);
    let crc = crc32(&output);
    if crc != crc_check {
        return Err(GZipErrorKind::CrcCheckFailed.into());
    }
    let i_size = u32::from_le_bytes((&buf[4 ..]).try_into()?);
    debug_assert_eq!(i_size, output.len() as u32);
    Ok(output)
}

pub fn gzip_encode<W:Write, T1:ToString, T2:ToString>(input:&[u8], modified_date:Option<DateTime>,
    file_name:T1, file_comment:T2, writer:&mut W) -> Result<(), GZipError>
{
    let header = GZipHeader::new(file_name, file_comment, modified_date);
    header.encode(writer)?;
    deflate_encode(input, writer)?;
    let crc = crc32(&input);
    writer.write(&crc.to_le_bytes())?;
    writer.write(&(input.len() as u32).to_le_bytes())?;
    Ok(())
}
