use core::fmt;
use std::error::Error;
use std::io;
use std::array::TryFromSliceError;
use crate::deflate::enums::deflate_error::DeflateError;

#[derive(Debug, Clone)]
pub enum GZipErrorKind {
    CrcCheckFailed,
    Parse,
    Inner
}

#[derive(Debug)]
pub struct GZipError {
    pub source:Option<Box<dyn Error>>,
    pub kind:GZipErrorKind,
}

impl GZipError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        GZipError {
            source: Some(source),
            kind: GZipErrorKind::Inner
        }
    }

    pub fn new_kind(kind:GZipErrorKind)->Self{
        GZipError {
            source: None,
            kind
        }
    }
}

impl Error for GZipError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl GZipErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:GZipError = self.into();
        error.into()
    }
}

impl fmt::Display for GZipErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            GZipErrorKind::CrcCheckFailed => write!(f, "crc check failed"),
            GZipErrorKind::Parse => write!(f, "parse error"),
            GZipErrorKind::Inner =>  write!(f, "error in GZipError::source"),
        }
    }
}

impl fmt::Display for GZipError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<GZipErrorKind> for GZipError {
    fn from(kind: GZipErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for GZipError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl From<io::Error> for GZipError {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<TryFromSliceError> for GZipError {
    fn from(error: TryFromSliceError) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<DeflateError> for GZipError {
    fn from(error: DeflateError) -> Self {
        Self::new_inner(error.into())
    }
}

