use std::collections::HashMap;
use crate::deflate::constants::{LITERAL_AND_LENGTH_ALPHABET, FIXED_CODES, FIXED_LENGTHS};
use core::fmt;
use std::fmt::{Formatter, Display};
use crate::deflate::utils::lengths_to_codes;

type Code = u64;

pub struct CodeToSymbolMap {
    codes:HashMap<usize, HashMap<Code, u64>>,
    pub lengths:Vec<usize>,
}

impl CodeToSymbolMap {
    pub fn new(lengths:Vec<usize>, alphabet:&[u64]) -> CodeToSymbolMap {
        let codes = lengths_to_codes::produce_codes(&lengths);
        Self::new_with_codes(lengths, alphabet, &codes)
    }

    fn new_with_codes(mut lengths:Vec<usize>, alphabet:&[u64], codes:&[u64]) -> Self{
        debug_assert_eq!(codes.len(), lengths.len());
        let mut map:HashMap<usize, HashMap<u64, u64>> = HashMap::with_capacity(lengths.len());
        for i in 0 .. codes.len() {
            if lengths[i] == 0 {
                continue;
            }
            match map.get_mut(&lengths[i]){
                Some(in_map) => {
                    in_map.insert(codes[i], alphabet[i]);
                },
                None => {
                    let mut in_map = HashMap::with_capacity(lengths.len());
                    in_map.insert(codes[i], alphabet[i]);
                    map.insert(lengths[i], in_map);
                }
            }
        }
        lengths.sort();
        lengths.dedup();
        if lengths[0] == 0 {
            lengths.remove(0);
        }

        CodeToSymbolMap {
            codes:map,
            lengths,
        }
    }

    pub fn new_fixed_codes() -> CodeToSymbolMap {
        Self::new_with_codes(Vec::from(FIXED_LENGTHS), LITERAL_AND_LENGTH_ALPHABET, &FIXED_CODES)
    }

    pub fn get(&self, (length, code):&(usize, u64)) -> Option<&u64> {
        match self.codes.get(&length) {
            Some(codes) => codes.get(&code),
            None => None,
        }
    }

}

impl std::ops::Index<(usize, u64)> for CodeToSymbolMap {
    type Output = u64;

    fn index(&self, (length, code): (usize, u64)) -> &Self::Output {
        self.codes.get(&length).unwrap().get(&code).unwrap()
    }
}

impl fmt::Display for CodeToSymbolMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "lengths: {:?}", &self.lengths)?;
        for len in &self.lengths {
            write!(f, "\t[{}]: ", len)?;
            for (code, value) in self.codes.get(&len).unwrap() {
                write!(f, " 0x{:02x}->{},", code, value)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl fmt::Debug for CodeToSymbolMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_produce_code_map(){
        let alphabet:&[u64] = &[1, 2, 3, 4, 5, 6, 7, 8];
        let map = CodeToSymbolMap::new(vec![3, 3, 3, 3, 3, 2, 4, 4], &alphabet);
        let mut value = 1u64;
        for code in 0b010 ..= 0b110 {
            assert_eq!(map[(3, code)], value);
            value += 1;
        }
        assert_eq!(map[(2, 0)], 6u64);
        let mut value = 7u64;
        for code in 0b1110 ..= 0b1111 {
            assert_eq!(map[(4, code)], value);
            value += 1;
        }

        let map = CodeToSymbolMap::new_fixed_codes();
        let mut value = 0u64;
        for code in 0b00110000 ..= 0b10111111 {
            assert_eq!(map[(8, code)], value);
            value += 1;
        }
        let mut value = 144u64;
        for code in 0b110010000 ..= 0b111111111 {
            assert_eq!(map[(9, code)], value);
            value += 1;
        }
        let mut value = 256u64;
        for code in 0b0000000 ..= 0b0010111 {
            assert_eq!(map[(7, code)], value);
            value += 1;
        }
        let mut value = 280u64;
        for code in 0b11000000 ..= 0b11000111 {
            assert_eq!(map[(8, code)], value);
            value += 1;
        }
    }

}



