use std::io::Read;
use core::cmp;
use crate::deflate::enums::deflate_error::DeflateError;

const BYTE_BITS:usize = 8;

pub struct BitReader<R:Read> {
    byte:[u8; 1],
    read:R,
    bits_available:usize,
}

impl<R:Read> BitReader<R> {
    pub fn new(read:R)->Self{
        BitReader{
            byte:[0],
            read,
            bits_available:0,
        }
    }

    pub fn read(&mut self, bits:usize) -> Result<(u64, usize), DeflateError> {
        debug_assert!(bits <= 64);
        let mut size_bits = 0;
        let mut result = 0u64;
        while (bits - size_bits) > 0 {
            if self.bits_available == 0 {
                match self.read_next(){
                    Ok(size) if size == 0 => break,
                    Ok(_) => (),
                    Err(err) => return Err(err),
                }
            }

            let take = cmp::min(bits - size_bits, self.bits_available);
            let byte = (!(0xff_usize << take)) as u8 & (self.byte[0] >> self.bit_used());
            result |= (byte as u64) << size_bits;
            size_bits += take;
            self.bits_available -= take;
        }
        Ok((result ,size_bits))
    }

    pub fn read_reversed(&mut self, bits:usize) -> Result<(u64, usize), DeflateError> {
        let (value, size) = self.read(bits)?;
        let value = value.reverse_bits() >> (64 - size);
        Ok((value, size))
    }

    fn bit_used(&self) -> usize{
        BYTE_BITS - self.bits_available
    }

    fn read_next(&mut self) -> Result<usize, DeflateError>{
        let size = self.read.read(&mut self.byte)?;
        self.bits_available = BYTE_BITS;
        Ok(size)
    }

    pub fn into_inner(self) -> R {
        self.read
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_read() -> Result<(), DeflateError>{
        let input = 0b01111000_11000010_01101001_10111001_u32;
        let input = input.to_le_bytes();
        // assert_eq!(i, [0b10111001, 0b01101001, 0b11000010, 0b01111000]);
        let mut bit_reader = BitReader::new(&input as &[u8]);
        assert_eq!(bit_reader.read(2)?, (1, 2));
        assert_eq!(bit_reader.read(2)?, (2, 2));
        assert_eq!(bit_reader.read(3)?, (3, 3));
        assert_eq!(bit_reader.read(4)?, (3, 4));
        assert_eq!(bit_reader.read(17)?, (0b1000_11000010_01101, 17));
        assert_eq!(bit_reader.read(10)?, (0b0111, 4));

        Ok(())
    }
}