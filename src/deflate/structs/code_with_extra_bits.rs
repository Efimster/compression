use core::fmt;
use std::fmt::{Formatter, Display};

#[derive(PartialEq, Copy, Clone)]
pub struct CodeWithExtraBits {
    pub code:u64,
    pub extra_bits:usize,
    pub extra_value:u64,
}

impl CodeWithExtraBits {
    pub fn new(code:u64, extra_bits:usize, extra_value:u64) -> Self{
        CodeWithExtraBits {code, extra_bits, extra_value}
    }
}

impl fmt::Display for CodeWithExtraBits {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}+{}[{}]", self.code, self.extra_value, self.extra_bits)
    }
}

impl fmt::Debug for CodeWithExtraBits {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}