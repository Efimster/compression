use std::collections::HashMap;
use crate::deflate::utils::lengths_to_codes;
use std::ops::Index;
use crate::deflate::constants::{FIXED_LENGTHS, LITERAL_AND_LENGTH_ALPHABET, FIXED_CODES};

type Code = u64;

#[derive(Debug)]
pub struct SymbolToCodeMap {
    symbols:HashMap<u64, (Code, usize)>,
}

impl SymbolToCodeMap {
    pub fn new(lengths:Vec<usize>, alphabet:&[u64]) -> Self {
        let codes = lengths_to_codes::produce_codes(&lengths);
        Self::new_with_codes(lengths, alphabet, &codes)
    }

    fn new_with_codes(lengths:Vec<usize>, alphabet:&[u64], codes:&[u64]) -> Self {
        debug_assert_eq!(codes.len(), lengths.len());
        let mut map:HashMap<u64, (Code, usize)> = HashMap::with_capacity(lengths.len());
        for i in 0 .. codes.len() {
            if lengths[i] == 0 {
                continue;
            }
            map.insert(alphabet[i], (codes[i], lengths[i]));
        }
        SymbolToCodeMap {
            symbols:map,
        }
    }

    pub fn new_fixed_codes() -> Self {
        Self::new_with_codes(Vec::from(FIXED_LENGTHS), LITERAL_AND_LENGTH_ALPHABET, &FIXED_CODES)
    }
}

impl Index<u64> for SymbolToCodeMap{
    type Output = (Code, usize);

    fn index(&self, index: u64) -> &Self::Output {
        &self.symbols[&index]
    }
}

