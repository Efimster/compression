use std::io::Write;
use crate::deflate::enums::deflate_error::DeflateError;
use core::cmp;

const REGISTER_LEN:usize = 64;

pub struct BitWriter<W:Write> {
    write:W,
    bit_pointer:usize,
    register:u64,
}

impl<W:Write> BitWriter<W> {
    pub fn new(write:W) -> Self{
        BitWriter {
            write,
            bit_pointer: 0,
            register: 0,
        }
    }

    pub fn write(&mut self, mut value:u64, mut bits:usize) -> Result<(), DeflateError>{
        while bits > 0 {
            let batch_len = cmp::min(REGISTER_LEN - self.bit_pointer, bits);
            self.register |= value << self.bit_pointer;
            // println!("register {:02x} left {} batch_len {} {:02x}", self.register, bits, batch_len, value);
            bits -= batch_len;
            value >>= batch_len;
            self.bit_pointer += batch_len;
            if self.bit_pointer == REGISTER_LEN {
                self.store_register()?;
            }
        }
        Ok(())
    }

    pub fn write_len(&mut self, length:usize, bits:usize) -> Result<(), DeflateError>{
        self.write(length as u64, bits)
    }

    fn store_register(&mut self) -> Result<(), DeflateError>{
        self.write.write(&self.register.to_le_bytes())?;
        self.bit_pointer = 0;
        self.register = 0;
        Ok(())
    }

    pub fn flush(&mut self) -> Result<(), DeflateError>{
        let len = (self.bit_pointer >> 3) + (self.bit_pointer > 0) as usize;
        self.write.write(&self.register.to_le_bytes()[.. len])?;
        self.register = 0;
        Ok(())
    }

    #[cfg(test)]
    pub fn into_inner(self) -> W {
        self.write
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_bit_writer_store() -> Result<(), DeflateError>{
        let output = Vec::new();
        let mut bit_writer = BitWriter::new( output);
        bit_writer.write(0, 62)?;
        bit_writer.write(0x1e, 5)?;
        bit_writer.flush()?;
        let output = bit_writer.into_inner();
        assert_eq!(output, [0x00 ,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x07]);
        Ok(())
    }
}