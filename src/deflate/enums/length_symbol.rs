use std::fmt::{Formatter, Display};
use core::fmt;

#[derive(Copy, Clone, PartialEq)]
pub enum LengthSymbol {
    Len(u64),
    Copy(usize),
    Zeros17(usize),
    Zeros18(usize),
}

impl LengthSymbol {
    pub fn symbol_index(&self) -> u64 {
        match self {
            LengthSymbol::Len(length) => *length,
            LengthSymbol::Copy(_) => 16,
            LengthSymbol::Zeros17(_) => 17,
            LengthSymbol::Zeros18(_) => 18,
        }
    }
}

impl fmt::Display for LengthSymbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            LengthSymbol::Len(length) => write!(f, "{}", length),
            LengthSymbol::Copy(times) => write!(f, "copy<{}>", times),
            LengthSymbol::Zeros17(times) => write!(f, "0<{}>", times),
            LengthSymbol::Zeros18(times) => write!(f, "0<{}>", times),
        }
    }
}

impl fmt::Debug for LengthSymbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}