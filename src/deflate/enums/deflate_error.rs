use core::fmt;
use std::error::Error;
use std::io;
use std::str::Utf8Error;
use std::string::FromUtf8Error;

#[derive(Debug, Clone)]
pub enum DeflateErrorKind {
    CodeNoteFound,
    HeaderParse,
    Inner
}

#[derive(Debug)]
pub struct DeflateError {
    pub source:Option<Box<dyn Error>>,
    pub kind:DeflateErrorKind,
}

impl DeflateError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        DeflateError {
            source: Some(source),
            kind: DeflateErrorKind::Inner
        }
    }

    pub fn new_kind(kind:DeflateErrorKind)->Self{
        DeflateError {
            source: None,
            kind
        }
    }
}

impl Error for DeflateError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl DeflateErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:DeflateError = self.into();
        error.into()
    }
}

impl fmt::Display for DeflateErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DeflateErrorKind::CodeNoteFound => write!(f, "code not found"),
            DeflateErrorKind::HeaderParse => write!(f, "could not recognize header"),
            DeflateErrorKind::Inner =>  write!(f, "error in DeflateError::source"),
        }
    }
}

impl fmt::Display for DeflateError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<DeflateErrorKind> for DeflateError {
    fn from(kind: DeflateErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for DeflateError {
    fn from(err: Box<dyn Error>) -> Self {
        Self::new_inner(err)
    }
}

impl From<io::Error> for DeflateError {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<Utf8Error> for DeflateError {
    fn from(error: Utf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<FromUtf8Error> for DeflateError {
    fn from(error: FromUtf8Error) -> Self {
        Self::new_inner(error.into())
    }
}