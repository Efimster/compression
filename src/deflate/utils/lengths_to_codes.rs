use std::collections::HashMap;
use crate::deflate::structs::code_with_extra_bits::CodeWithExtraBits;
use crate::deflate::constants::{LENGTH_SYMBOLS_EXTRA, DISTANCE_SYMBOLS_EXTRA, CODE_LENGTH_SYMBOL_EXTRA};

fn b1_count(lengths:&[usize]) -> (HashMap<usize, usize>, usize){
    let mut counts = HashMap::with_capacity(lengths.len());
    let mut max_bits = 0usize;
    for i in lengths {
        match counts.get_mut(i) {
            Some(count) => *count += 1,
            None => {counts.insert(*i, 1usize);},
        }
        if &max_bits < i {
            max_bits = *i;
        }
    }
    (counts, max_bits)
}

fn next_code(max_bits:usize, counts:&HashMap<usize, usize>) -> HashMap<usize, u64>{
    let mut code = 0;
    let mut next_codes = HashMap::with_capacity(counts.capacity() + 1);
    for bits in 0 .. max_bits {
        code += match counts.get(&bits) {
            Some(count) => *count as u64,
            None => 0,
        };
        code <<= 1;
        next_codes.insert(bits + 1, code as u64);
    }
    next_codes
}

fn code_values(lengths:&[usize], mut next_codes:HashMap<usize, u64>) -> Vec<u64>{
    let mut codes = Vec::with_capacity(lengths.len());
    for len in lengths{
        if len != &0 {
            let code_ref = next_codes.get_mut(len).unwrap();
            codes.push(*code_ref);
            *code_ref += 1;
        }
        else {
            codes.push(0);
        }
    }
    codes
}

pub fn produce_codes(lengths:&[usize]) -> Vec<u64> {
    let (mut counts, max_bits) = b1_count(lengths);
    counts.insert(0, 0);
    let next_codes = next_code(max_bits, &counts);
    code_values(lengths, next_codes)
}

///length 258 removed (see MAX_MATCH_LEN) as inconsistent to the algorithm and rare
pub fn length_to_code(length:usize) -> CodeWithExtraBits {
    let length = length - 3;
    if length < 8 {
        return CodeWithExtraBits::new(length as u64 + 257, 0, 0);
    }
    let mut next_base = 1;
    while next_base <= length {
        next_base <<= 1;
    }
    let base = next_base >> 1;
    let base_zeros = base.trailing_zeros() as usize;
    let quoter = (next_base - base) >> 2;
    let quoter_num = (length - base) / quoter;
    let code = (base_zeros << 2) + quoter_num;
    // dbg!(length, code, base, next_base, quoter, quoter_num, base_zeros);
    CodeWithExtraBits::new(code as u64 + 253, base_zeros - 2, (length - base - quoter * quoter_num) as u64)
}

pub fn code_to_length(code_with_extra_bits:&CodeWithExtraBits) -> usize {
    if code_with_extra_bits.code < 265 {
        return code_with_extra_bits.code as usize - 254;
    }
    let mut base = 1usize << code_with_extra_bits.extra_bits + 2;
    let quoter = (base << 1) - base >> 2;
    base += (code_with_extra_bits.code as usize - 265 - ((code_with_extra_bits.extra_bits - 1) << 2)) * quoter;
    base + code_with_extra_bits.extra_value as usize + 3
}

// pub fn distance_to_code(distance:usize) -> usize {
//     DISTANCE_WEIGHT_RANGES.binary_search_by(|range|{
//         if *range.end() < distance {
//             Ordering::Less
//         }
//         else if *range.start() > distance {
//             Ordering::Greater
//         }
//         else {
//             Ordering::Equal
//         }
//     }).unwrap()
// }

pub fn distance_to_code(distance:usize) -> CodeWithExtraBits {
    let distance = distance - 1;
    if distance < 4 {
        return CodeWithExtraBits::new(distance as u64, 0, 0)
    }
    let mut next_base = 1;
    while next_base <= distance {
        next_base <<= 1;
    }
    let base = next_base >> 1;
    let base_zeros = base.trailing_zeros() as usize;
    let half = (next_base - base) >> 1;
    let second_half = (distance - base) / half;
    let code = (base_zeros << 1) + second_half;
    CodeWithExtraBits::new(code as u64,  base_zeros - 1, (distance - base - half * second_half) as u64)
}

pub fn code_to_distance(code_with_extra_bits:&CodeWithExtraBits) -> usize {
    if code_with_extra_bits.code < 4 {
        return code_with_extra_bits.code as usize + 1;
    }
    let mut base = 1usize << code_with_extra_bits.extra_bits + 1;
    let half = (base << 1) - base >> 1;
    base += (code_with_extra_bits.code as usize - 4 - ((code_with_extra_bits.extra_bits - 1) << 1)) * half;
    base + code_with_extra_bits.extra_value as usize + 1
}

pub fn length_symbol_to_extra_bits(symbol:usize) -> usize {
    if symbol < 265 {
        return 0;
    }
    LENGTH_SYMBOLS_EXTRA[symbol - 257].1
}

pub fn distance_symbol_to_extra_bits(symbol:usize) -> usize {
    DISTANCE_SYMBOLS_EXTRA[symbol].1
}

pub fn code_length_symbol_to_extra_bits(symbol:usize) -> usize {
    CODE_LENGTH_SYMBOL_EXTRA[symbol]
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::deflate::constants::FIXED_CODES;

    #[test]
    fn test_produce_codes() {
        let codes = produce_codes(&[3, 3, 3, 3, 3, 2, 4, 4]);
        assert_eq!(codes, [0b010, 0b011, 0b100, 0b101, 0b110, 0b00, 0b1110, 0b1111]);

        let mut lengths = Vec::with_capacity(287);
        lengths.extend_from_slice(&[8; 144]);
        lengths.extend_from_slice(&[9; 112]);
        lengths.extend_from_slice(&[7; 24]);
        lengths.extend_from_slice(&[8; 8]);
        let codes = produce_codes(&lengths);
        assert_eq!(codes.len(), 288);
        assert_eq!(codes, FIXED_CODES);
    }

    #[test]
    fn test_distance_to_code() {
        assert_eq!(distance_to_code(5), CodeWithExtraBits::new(4, 1, 0));
        assert_eq!(distance_to_code(6), CodeWithExtraBits::new(4, 1, 1));
        assert_eq!(distance_to_code(7), CodeWithExtraBits::new(5, 1, 0));
        assert_eq!(distance_to_code(8), CodeWithExtraBits::new(5, 1, 1));
        assert_eq!(distance_to_code(9), CodeWithExtraBits::new(6, 2, 0));
        assert_eq!(distance_to_code(10), CodeWithExtraBits::new(6, 2, 1));
        assert_eq!(distance_to_code(11), CodeWithExtraBits::new(6, 2, 2));
        assert_eq!(distance_to_code(12), CodeWithExtraBits::new(6, 2, 3));
        assert_eq!(distance_to_code(1), CodeWithExtraBits::new(0, 0, 0));
        assert_eq!(distance_to_code(2), CodeWithExtraBits::new(1, 0, 0));
        assert_eq!(distance_to_code(3), CodeWithExtraBits::new(2, 0, 0));
        assert_eq!(distance_to_code(4), CodeWithExtraBits::new(3, 0, 0));
        assert_eq!(distance_to_code(25), CodeWithExtraBits::new(9, 3, 0));
        assert_eq!(distance_to_code(32), CodeWithExtraBits::new(9, 3, 7));
        assert_eq!(distance_to_code(12289), CodeWithExtraBits::new(27, 12, 0));
        assert_eq!(distance_to_code(24577), CodeWithExtraBits::new(29, 13, 0));
        assert_eq!(distance_to_code(32768), CodeWithExtraBits::new(29, 13, 8191));
    }

    #[test]
    fn test_length_to_code() {
        assert_eq!(length_to_code(3), CodeWithExtraBits::new(257, 0, 0));
        assert_eq!(length_to_code(4), CodeWithExtraBits::new(258, 0, 0));
        assert_eq!(length_to_code(10), CodeWithExtraBits::new(264, 0, 0));
        assert_eq!(length_to_code(11), CodeWithExtraBits::new(265, 1, 0));
        assert_eq!(length_to_code(12), CodeWithExtraBits::new(265, 1, 1));
        assert_eq!(length_to_code(13), CodeWithExtraBits::new(266, 1, 0));
        assert_eq!(length_to_code(14), CodeWithExtraBits::new(266, 1, 1));
        assert_eq!(length_to_code(15), CodeWithExtraBits::new(267, 1, 0));
        assert_eq!(length_to_code(16), CodeWithExtraBits::new(267, 1, 1));
        assert_eq!(length_to_code(17), CodeWithExtraBits::new(268, 1, 0));
        assert_eq!(length_to_code(18), CodeWithExtraBits::new(268, 1, 1));
        assert_eq!(length_to_code(19), CodeWithExtraBits::new(269, 2, 0));
        assert_eq!(length_to_code(34), CodeWithExtraBits::new(272, 2, 3));
        assert_eq!(length_to_code(195), CodeWithExtraBits::new(283, 5, 0));
        assert_eq!(length_to_code(257), CodeWithExtraBits::new(284, 5, 30));
        // assert_eq!(length_to_code(258), (285, 0, 0)); this value removed as inconsistent and rare, see MAX_MATCH_LEN
    }

    #[test]
    fn test_code_to_distance() {
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(4, 1, 0)), 5);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(4, 1, 1)), 6);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(5, 1, 0)), 7);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(5, 1, 1)), 8);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(6, 2, 0)), 9);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(6, 2, 1)), 10);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(6, 2, 2)), 11);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(6, 2, 3)), 12);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(0, 0, 0)), 1);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(1, 0, 0)), 2);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(2, 0, 0)), 3);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(3, 0, 0)), 4);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(9, 3, 0)), 25);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(9, 3, 7)), 32);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(27, 12, 0)), 12289);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(29, 13, 0)), 24577);
        assert_eq!(code_to_distance(&CodeWithExtraBits::new(29, 13, 8191)), 32768);
    }

    #[test]
    fn test_code_to_length() {
        assert_eq!(code_to_length(&CodeWithExtraBits::new(257, 0, 0)), 3);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(258, 0, 0)), 4);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(264, 0, 0)), 10);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(265, 1, 0)), 11);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(265, 1, 1)), 12);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(266, 1, 0)), 13);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(266, 1, 1)), 14);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(267, 1, 0)), 15);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(267, 1, 1)), 16);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(268, 1, 0)), 17);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(268, 1, 1)), 18);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(269, 2, 0)), 19);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(272, 2, 3)), 34);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(283, 5, 0)), 195);
        assert_eq!(code_to_length(&CodeWithExtraBits::new(284, 5, 30)), 257);
    }
}