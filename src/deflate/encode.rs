use crate::lz77::lz77::LZ77;
use crate::deflate::constants::{CODE_LENGTH_ALPHABET, LITERAL_AND_LENGTH_ALPHABET, DISTANCE_ALPHABET, CODE_LENGTH_ALPHABET_ENCODE_ORDER};
use crate::huffman_code::huffman_encoder::HuffmanEncoder;
use crate::deflate::structs::bit_writer::BitWriter;
use crate::deflate::enums::length_symbol::LengthSymbol;
use crate::deflate::enums::deflate_error::DeflateError;
use crate::deflate::utils::lengths_to_codes;
use crate::deflate::structs::symbol_to_code_map::SymbolToCodeMap;
use std::io::Write;
use crate::lz77::lz77_encode::{LAZY_MATCH_LEVEL_0, input_to_lz77};

pub fn deflate_encode<W:Write>(input:&[u8], output:&mut W) -> Result<(), DeflateError>{
    deflate_encode_with_lazy_match(input, output, LAZY_MATCH_LEVEL_0)
}

/// LAZY_MATCH levels 1 and 2 didn't show better compression (with observed examples) while require more CPU cycles
pub fn deflate_encode_with_lazy_match<W:Write>(input:&[u8], output:&mut W, lazy_match:usize) -> Result<(), DeflateError>{
    let (lz77_input, literal_length_weights, distance_weights)
        = input_to_lz77(input, lazy_match);
    let fixed_bits = calculate_fixed_huffman_codes_bits(&literal_length_weights, &distance_weights);
    let (dynamic_bits, literal_length_tree, distance_tree,
        code_length_tree, trees_compact_representation)
            = calculate_dynamic_huffman_codes_bits(&literal_length_weights, &distance_weights);
    let non_compressed_bits = calculate_non_compressed_bits(input.len());
    // println!("fixed_bits {}, dynamic_bits {} non compressed {}", fixed_bits, dynamic_bits, non_compressed_bits);
    if dynamic_bits < fixed_bits && dynamic_bits < non_compressed_bits {
        encode_dynamic(&lz77_input, output, literal_length_tree, distance_tree, code_length_tree, trees_compact_representation)?;
    }
    else if fixed_bits < non_compressed_bits {
        encode_fixed(&lz77_input, output)?;
    }
    else {
        encode_non_compressed(input, output)?;
    }
    Ok(())
}

fn calculate_fixed_huffman_codes_bits(literal_length_weights:&[usize; 286], distance_weights:&[usize; 30]) -> usize{
    let mut total_bits = literal_length_weights[0 ..= 143].iter().fold(0usize, |acc, &x| acc + x) << 3;
    total_bits += literal_length_weights[144 ..= 255].iter().fold(0usize, |acc, &x| acc + x) * 9;
    total_bits += literal_length_weights[256 ..= 264].iter().fold(0usize, |acc, &x| acc + x) * 7;
    for i in 265 ..= 279  {
        total_bits += (7 + lengths_to_codes::length_symbol_to_extra_bits(i)) * literal_length_weights[i];
    }
    for i in 280 ..= 285  {
        total_bits += (8 + lengths_to_codes::length_symbol_to_extra_bits(i)) * literal_length_weights[i];
    }
    for i in 0 .. distance_weights.len() {
        total_bits += (5 + lengths_to_codes::distance_symbol_to_extra_bits(i)) * distance_weights[i];
    }
    total_bits + 3
}

fn calculate_dynamic_huffman_codes_bits(literal_length_weights:&[usize], distance_weights:&[usize])
    -> (usize, Vec<usize>, Vec<usize>, Vec<usize>, Vec<LengthSymbol>)
{
    let literal_length_tree = HuffmanEncoder::new(&literal_length_weights).into_code_lengths();
    let distance_tree = HuffmanEncoder::new(&distance_weights).into_code_lengths();
    // println!("huffman literal_length lengths ({}) {:?}", literal_length_tree.len(), &literal_length_tree);
    // println!("huffman distance lengths ({}) {:?}", distance_tree.len(), &distance_tree);

    let mut all_lengths = Vec::with_capacity(literal_length_tree.len() + distance_tree.len());
    all_lengths.extend_from_slice(&literal_length_tree);
    all_lengths.extend_from_slice(&distance_tree);
    let (all_lengths, code_length_weights) = lengths_to_length_alphabet(&all_lengths);
    // println!("code length alphabet weights {:?}", &code_length_weights);
    // println!("code length alphabet symbols {:?}", &all_lengths);
    let code_length_tree = HuffmanEncoder::new(&code_length_weights).into_code_lengths();
    // println!("code length tree ({}) {:?}", code_length_tree.len(), &code_length_tree);

    let mut total_bits = 14;//hlit + hdist + hclen

    for i in 0 .. code_length_tree.len() {
        total_bits += (code_length_tree[i] + lengths_to_codes::code_length_symbol_to_extra_bits(i)) * code_length_weights[i] + 3;
    }
    for i in 0 .. literal_length_tree.len() {
        total_bits += (literal_length_tree[i] + lengths_to_codes::length_symbol_to_extra_bits(i)) * literal_length_weights[i];
    }
    for i in 0 .. distance_tree.len() {
        total_bits += (distance_tree[i] + lengths_to_codes::distance_symbol_to_extra_bits(i)) * distance_weights[i];
    }

    (total_bits, literal_length_tree, distance_tree, code_length_tree, all_lengths)
}

fn lengths_to_length_alphabet(lengths:&[usize]) -> (Vec<LengthSymbol>, [usize; CODE_LENGTH_ALPHABET.len()]){
    let mut lengths_converted = Vec::with_capacity(lengths.len());
    let mut weights = [0usize; CODE_LENGTH_ALPHABET.len()];
    let mut i = 0usize;
    while i < lengths.len() {
        let mut j = i + 1;
        if lengths[i] == 0 {
            while j < lengths.len() && lengths[j] == 0{
                j += 1;
            }
            let mut zeros = j - i;
            while zeros > 0 {
                if zeros < 3 {
                    weights[0] += zeros;
                    for _ in 0 .. zeros {
                        lengths_converted.push(LengthSymbol::Len(0));
                    }
                    zeros = 0;
                }
                else if zeros < 11 {
                    weights[17] += 1;
                    lengths_converted.push(LengthSymbol::Zeros17(zeros));
                    zeros = 0;
                }
                else if zeros < 139{
                    weights[18] += 1;
                    lengths_converted.push(LengthSymbol::Zeros18(zeros));
                    zeros = 0;
                }
                else {
                    weights[18] += 1;
                    lengths_converted.push(LengthSymbol::Zeros18(138));
                    zeros -= 138;
                }
            }
        }
        else {
            let replica =  lengths[i];
            weights[replica] += 1;
            lengths_converted.push(LengthSymbol::Len(replica as u64));
            i += 1;
            while j < lengths.len() && lengths[j] == replica{
                j += 1;
            }
            let mut replica_len = j - i;
            while replica_len > 0 {
                if replica_len < 3 {
                    weights[replica] += replica_len;
                    for _ in 0 .. replica_len {
                        lengths_converted.push(LengthSymbol::Len(replica as u64));
                    }
                    replica_len = 0;
                }
                else if replica_len < 7 {
                    weights[16] += 1;
                    lengths_converted.push(LengthSymbol::Copy(replica_len));
                    replica_len = 0;
                }
                else {
                    weights[16] += 1;
                    lengths_converted.push(LengthSymbol::Copy(6));
                    replica_len -= 6;
                }
            }
        }
        i = j;
    }
    (lengths_converted, weights)
}

fn calculate_non_compressed_bits(input_bytes_len:usize) -> usize {
    (5 + input_bytes_len) << 3
}

fn encode_fixed<W:Write>(input:&[LZ77], writer:&mut W) -> Result<(), DeflateError> {
    let mut bit_writer = BitWriter::new( writer);
    bit_writer.write(1, 1)?;
    bit_writer.write(1, 2)?;
    let literal_length_map = SymbolToCodeMap::new_fixed_codes();
    for symbol in input {
        match symbol {
            LZ77::Lit(symbol_index) => {
                write_code(*symbol_index as u64, &mut bit_writer, &literal_length_map)?;
            },
            LZ77::Match(length, distance) => {
                write_code(length.code, &mut bit_writer, &literal_length_map)?;
                bit_writer.write(length.extra_value, length.extra_bits)?;
                let code = distance.code.reverse_bits() as usize >> 59;//59 = 64 - 5
                bit_writer.write_len(code, 5)?;
                bit_writer.write(distance.extra_value, distance.extra_bits)?;
            }
        }
    }
    write_code(256, &mut bit_writer, &literal_length_map)?;
    bit_writer.flush()?;
    Ok(())
}

fn encode_non_compressed<W:Write>(input:&[u8], writer:&mut W) -> Result<(), DeflateError> {
    writer.write(&[0b001])?;
    writer.write(&(input.len() as u16).to_le_bytes())?;
    writer.write(&((!input.len()) as u16).to_le_bytes())?;
    writer.write(input)?;
    Ok(())
}

fn encode_dynamic<W:Write>(input:&[LZ77], writer:&mut W, literal_length_tree:Vec<usize>, distance_tree:Vec<usize>,
    code_length_tree:Vec<usize>, trees_compact_representation:Vec<LengthSymbol>) -> Result<(), DeflateError>
{
    let mut bit_writer = BitWriter::new( writer);
    bit_writer.write(1, 1)?;
    bit_writer.write(2, 2)?;
    bit_writer.write_len(literal_length_tree.len() - 257, 5)?;
    bit_writer.write_len(distance_tree.len() - 1, 5)?;
    let reordered_code_length_tree = reorder_code_length_tree(&code_length_tree);
    bit_writer.write_len(reordered_code_length_tree.len() - 4, 4)?;
    for i in 0 .. reordered_code_length_tree.len() {
        bit_writer.write_len(reordered_code_length_tree[i], 3)?;
    }
    // println!("code length tree ({}) {:?}", code_length_tree.len(), &code_length_tree);
    let code_length_map = SymbolToCodeMap::new(code_length_tree, CODE_LENGTH_ALPHABET);
    // println!("code length map {:?}", &code_length_map);
    for length_symbol in &trees_compact_representation{
        write_code(length_symbol.symbol_index(), &mut bit_writer, &code_length_map)?;
        match length_symbol {
            LengthSymbol::Len(_) => (),
            LengthSymbol::Copy(times) =>
                bit_writer.write_len(times - 3, 2)?,
            LengthSymbol::Zeros17(times) =>
                bit_writer.write_len(times - 3, 3)?,
            LengthSymbol::Zeros18(times) =>
                bit_writer.write_len(times - 11, 7)?,
        }
    }
    let literal_length_map = SymbolToCodeMap::new(literal_length_tree, LITERAL_AND_LENGTH_ALPHABET);
    let distance_map = SymbolToCodeMap::new(distance_tree, DISTANCE_ALPHABET);
    for symbol in input {
        match symbol {
            LZ77::Lit(symbol_index) => {
                write_code(*symbol_index as u64, &mut bit_writer, &literal_length_map)?;
            },
            LZ77::Match(length, distance) => {
                write_code(length.code, &mut bit_writer, &literal_length_map)?;
                bit_writer.write(length.extra_value, length.extra_bits)?;
                write_code(distance.code, &mut bit_writer, &distance_map)?;
                bit_writer.write(distance.extra_value, distance.extra_bits)?;
            }
        }
    }
    write_code(256, &mut bit_writer, &literal_length_map)?;
    bit_writer.flush()?;
    Ok(())
}

fn reorder_code_length_tree(code_length_tree:&[usize]) -> Vec<usize>{
    let mut reordered = Vec::with_capacity(code_length_tree.len());
    for i in 0 .. code_length_tree.len() {
        reordered.push(code_length_tree[CODE_LENGTH_ALPHABET_ENCODE_ORDER[i]]);
    }
    while reordered[reordered.len() - 1] == 0 {
        reordered.pop();
    }
    reordered
}

fn write_code<W:Write>(symbol:u64, bit_writer:&mut BitWriter<W>, map:&SymbolToCodeMap) -> Result<(), DeflateError>{
    let (code, bits) = map[symbol];
    // println!("write code {} bits {} = symbol {}", code, bits, symbol);
    let code = code.reverse_bits() >> (64 - bits);
    // println!("write code {} bits {} = symbol {}", code, bits, symbol);
    bit_writer.write(code, bits)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_deflate_encode_dynamic() -> Result<(), DeflateError>{
        let mut output = Vec::new();
        let input = "This is the tool where you can compress your text or decompress your compressed value using Gzip(gz), Defalte or Brotli compression algorithms. Also you can compare the size between your source and result in bytes.";
        deflate_encode(input.as_bytes(), &mut output)?;
        // let s_output = output.iter().map(|i|format!("{:#04x}", i)).collect::<Vec<_>>().join(", ");
        assert_eq!(output, [0x55, 0x4e, 0x6d, 0x0a, 0x85, 0x20, 0x10, 0xbc, 0xca, 0xfe, 0x7c, 0x0f, 0x1e, 
            0xdd, 0xe1, 0x45, 0xd0, 0x05, 0xba, 0x80, 0xd9, 0xa6, 0x82, 0xb9, 0xe1, 0xae, 0x7d, 0x78, 0xfa, 
            0x8c, 0x28, 0x08, 0x86, 0x81, 0x19, 0x98, 0x8f, 0xce, 0x3a, 0x86, 0x02, 0xb1, 0x08, 0x42, 0xe4, 
            0x61, 0xb5, 0x18, 0x11, 0x76, 0x4a, 0xa0, 0x55, 0x00, 0x4d, 0xd3, 0x1c, 0x91, 0xf9, 0x34, 0x22, 
            0x08, 0x6e, 0x02, 0x14, 0x61, 0xc0, 0xb7, 0x7f, 0x2b, 0x1c, 0x60, 0x51, 0x3e, 0x21, 0x24, 0x76, 
            0xc1, 0x40, 0x9b, 0xdd, 0xfc, 0x31, 0xf9, 0xfb, 0x83, 0x06, 0x47, 0xe5, 0x05, 0xcf, 0x6c, 0x1d, 
            0x49, 0xbc, 0x7b, 0x22, 0x8e, 0x02, 0x28, 0x6f, 0x28, 0x3a, 0xb1, 0x13, 0x57, 0xf0, 0xf7, 0x4c, 
            0xaf, 0x75, 0x55, 0xde, 0x9c, 0xe7, 0xd8, 0x65, 0x84, 0x1e, 0x65, 0x45, 0x0c, 0xd7, 0x2a, 0x17, 
            0xd2, 0x08, 0x2a, 0x0c, 0x50, 0x9a, 0x92, 0x17, 0x70, 0x01, 0xfa, 0x5d, 0x90, 0xab, 0x03]);
        Ok(())
    }

    #[test]
    fn test_weights_for_code_lengths_alphabet(){
        let lengths = &[0, 8, 8, 8, 8, 8, 8, 8, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4,
            5, 5, 5, 5,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1,
            0, 0, 0];
        let (converted, weights) = lengths_to_length_alphabet(lengths);
        assert_eq!(&weights, &[3, 2, 2, 3, 3, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 3, 1, 1]);
        assert_eq!(&converted as &[LengthSymbol], &[LengthSymbol::Len(0), LengthSymbol::Len(8), LengthSymbol::Copy(6),
            LengthSymbol::Len(2), LengthSymbol::Len(2), LengthSymbol::Len(3), LengthSymbol::Copy(6),
            LengthSymbol::Len(3), LengthSymbol::Len(3), LengthSymbol::Len(4), LengthSymbol::Len(4), LengthSymbol::Len(4),
            LengthSymbol::Len(5), LengthSymbol::Copy(3), LengthSymbol::Zeros18(11), LengthSymbol::Len(1),
            LengthSymbol::Len(0), LengthSymbol::Len(0), LengthSymbol::Len(1), LengthSymbol::Zeros17(3),
        ] as &[LengthSymbol]);
    }

    #[test]
    fn test_deflate_encode_fixed() -> Result<(), DeflateError>{
        let mut output = Vec::new();
        let input = "abcdefghijklmnop abcdefghijklmnop,qrstuvw";
        deflate_encode(input.as_bytes(), &mut output)?;
        // let s_output = output.iter().map(|i|format!("{:#04x}", i)).collect::<Vec<_>>().join(", ");
        // println!("output ({}) {}", output.len(), s_output);
        assert_eq!(output, [0x4b, 0x4c, 0x4a, 0x4e, 0x49, 0x4d, 0x4b, 0xcf, 0xc8, 0xcc, 0xca, 0xce, 0xc9, 0xcd, 0xcb, 0x2f, 0x50, 0x40, 0x17, 0xd0, 0x29, 0x2c, 0x2a, 0x2e, 0x29, 0x2d, 0x2b, 0x07, 0x00]);
        Ok(())
    }

    #[test]
    fn test_deflate_encode_non_compressed() -> Result<(), DeflateError>{
        let mut output = Vec::new();
        let input = "a";
        encode_non_compressed(input.as_bytes(), &mut output)?;
        // let s_output = output.iter().map(|i|format!("{:#04x}", i)).collect::<Vec<_>>().join(", ");
        // println!("output ({}) {}", output.len(), s_output);
        assert_eq!(output, [0x01, 0x01, 0x00, 0xfe, 0xff, 0x61]);
        Ok(())
    }
}