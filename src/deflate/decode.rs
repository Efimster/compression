use std::io::{Read, Write};
use crate::deflate::structs::bit_reader::BitReader;
use crate::deflate::enums::deflate_error::{DeflateError, DeflateErrorKind};
use core::cmp;
use crate::deflate::structs::code_to_symbol_map::CodeToSymbolMap;
use crate::deflate::constants::{CODE_LENGTH_ALPHABET, DISTANCE_SYMBOLS_EXTRA, LENGTH_SYMBOLS_EXTRA, DISTANCE_ALPHABET, LITERAL_AND_LENGTH_ALPHABET, CODE_LENGTH_ALPHABET_ENCODE_ORDER};

pub fn deflate_decode<R:Read + ?Sized>(input:&mut R, output:&mut Vec<u8>) -> Result<(), DeflateError>{
    let mut reader = BitReader::new(input);

    loop {
        let (final_block, size) = reader.read(1)?;
        debug_assert_eq!(size, 1);
        match reader.read(2)? {
            (0b00, 2) => {
                let mut input = reader.into_inner();
                decode_non_compressed_compression(&mut input, output)?;
                reader = BitReader::new(input);
            },
            (0b01, 2) => {
                let codes = CodeToSymbolMap::new_fixed_codes();
                decode_fixed_codes_compression(&mut reader, &codes, output)?;
            },
            (0b10, 2) => {
                decode_dynamic_codes_compression(&mut reader, output)?;
            },
            _ => return Err(DeflateErrorKind::HeaderParse.into()),
        }
        if final_block == 1 {
            break;
        }
    }

    Ok(())
}

fn decode_fixed_codes_compression<R:Read>(reader:&mut BitReader<R>, codes:&CodeToSymbolMap, output:&mut Vec<u8>) -> Result<(), DeflateError> {
    loop {
        let value = decode_next_value(reader, codes)?;
        debug_assert!(value < 286);
        if value == 256 {
            break;
        }
        else if value < 256 {
            output.write(&[value as u8])?;
        }
        else {
            let (length, distance) = decode_length_distance_fixed(value, reader)?;
            repeat(length, distance, output);
        }
    }
    Ok(())
}

fn repeat(mut length:usize, distance:usize, output:&mut Vec<u8>){
    let from = &output[output.len() - distance ..];
    let mut to_repeat = Vec::with_capacity(length);
    loop {
        let len = cmp::min(length, distance);
        to_repeat.extend_from_slice(&from[ .. len]);
        length -= len;
        if length == 0 {
            break;
        }
    }
    output.extend_from_slice(&to_repeat);
}

fn decode_next_value<R:Read>(reader:&mut BitReader<R>, codes:&CodeToSymbolMap) -> Result<u64, DeflateError> {
    let mut last_len = 0;
    let mut code = 0;
    for &code_length in &codes.lengths {
        let len = code_length - last_len;
        let (bits, size) = reader.read_reversed(len)?;
        debug_assert_eq!(size, len);
        code = (code << len) | bits;
        // println!("bits({}): 0x{:02x}", code_length, code);
        if let Some(value) =  codes.get(&(code_length, code)) {
            // println!("code!!!: {:02x} {}", code, value);
            return Ok(*value);
        }
        last_len = code_length;
    }
    Err(DeflateErrorKind::CodeNoteFound.into())
}

fn get_repetitive_length<R:Read>(code:u64, reader:&mut BitReader<R>) -> Result<u64, DeflateError> {
    let (base, extra_bits) = LENGTH_SYMBOLS_EXTRA[code as usize - 257];
    let (extra_value, size) = reader.read(extra_bits)?;
    debug_assert_eq!(size, extra_bits);
    Ok(base + extra_value)
}

fn get_distance<R:Read>(code:u64, reader:&mut BitReader<R>) -> Result<u64, DeflateError> {
    let (base, extra_bits) = DISTANCE_SYMBOLS_EXTRA[code as usize];
    let (extra_value, size) = reader.read(extra_bits)?;
    debug_assert_eq!(size, extra_bits);
    Ok(base + extra_value)
}

fn decode_length_distance_fixed<R:Read>(length_code:u64, reader:&mut BitReader<R>) -> Result<(usize, usize), DeflateError>{
    let length = get_repetitive_length(length_code, reader)?;
    let (code, size) = reader.read_reversed(5)?;
    debug_assert_eq!(size, 5);
    let distance = get_distance(code, reader)?;
    Ok((length as usize, distance as usize))
}

fn decode_length_distance_dynamic<R:Read>(length_code:u64, reader:&mut BitReader<R>, distance_code_map:&CodeToSymbolMap)
    -> Result<(usize, usize), DeflateError>
{
    let length = get_repetitive_length(length_code, reader)?;
    let code = decode_next_value(reader, distance_code_map)?;
    let distance = get_distance(code, reader)?;
    Ok((length as usize, distance as usize))
}

fn decode_dynamic_codes_compression<R:Read>(reader:&mut BitReader<R>, output:&mut Vec<u8>) -> Result<(), DeflateError>{
    let (hlit, size) = reader.read(5)?;
    let hlit = hlit as usize + 257;
    debug_assert_eq!(size, 5);
    let (hdist, size) = reader.read(5)?;
    let hdist = hdist as usize + 1;
    debug_assert_eq!(size, 5);
    let (hclen, size) = reader.read(4)?;
    debug_assert_eq!(size, 4);
    let hclen = hclen as usize + 4;
    let mut code_length_tree = vec![0usize; CODE_LENGTH_ALPHABET.len()];
    for i in 0 .. hclen {
        let (length, size) = reader.read(3)?;
        debug_assert_eq!(size, 3);
        code_length_tree[CODE_LENGTH_ALPHABET_ENCODE_ORDER[i]] = length as usize;
    }
    // println!("hlit={}, hdist={}, hclen={}", hlit, hdist, hclen);
    // println!("code length tree ({}) {:?}", code_length_tree.len(), &code_length_tree);
    let code_length_map = CodeToSymbolMap::new(code_length_tree, &CODE_LENGTH_ALPHABET);
    // println!("CODE_LENGTH_ALPHABET map {:?}", &code_length_map);
    let mut lengths = decode_code_lengths(reader, hlit + hdist, &code_length_map)?;
    let distance_tree = lengths.drain(hlit ..).collect::<Vec<_>>();
    // println!("lit|length tree ({}) {:?}", lengths.len(), &lengths);
    // println!("distance tree ({}) {:?}", distance_tree.len(), &distance_tree);
    let literal_length_map = CodeToSymbolMap::new(lengths, &LITERAL_AND_LENGTH_ALPHABET);
    // println!("LITERAL_AND_LENGTH_ALPHABET map {:?}", &literal_length_map);
    let distance_map = CodeToSymbolMap::new(distance_tree, &DISTANCE_ALPHABET);
    // println!("DISTANCE_ALPHABET map {:?}", &distance_map);
    loop {
        let value = decode_next_value(reader, &literal_length_map)?;
        debug_assert!(value < 286);
        if value == 256 {
            break;
        }
        else if value < 256 {
            output.write(&[value as u8])?;
        }
        else {
            let (length, distance) = decode_length_distance_dynamic(value, reader, &distance_map)?;
            // println!("[{}] <{},{}> \"{}\"", c, length, distance,
            //          std::str::from_utf8(&output[output.len() - distance .. output.len() - distance + length]).unwrap());
            repeat(length, distance, output);
        }
    }

    Ok(())
}

fn decode_code_lengths<R:Read>(reader:&mut BitReader<R>, expected_lengths_count:usize, codes:&CodeToSymbolMap) -> Result<Vec<usize>, DeflateError>{
    let mut lengths = Vec::with_capacity(expected_lengths_count);
    while lengths.len() < expected_lengths_count {
        let value = decode_next_value(reader, codes)?;
        match value {
            value@0 ..= 15 => lengths.push(value as usize),
            16 => {
                let (extra, size)= reader.read(2)?;
                debug_assert_eq!(size, 2);
                let to_repeat = lengths[lengths.len() - 1] as usize;
                for _ in 0 .. extra + 3 {
                    lengths.push(to_repeat);
                }
            },
            17 => {
                let (mut extra, size)= reader.read(3)?;
                debug_assert_eq!(size, 3);
                extra += 3;
                while extra > 0 {
                    lengths.push(0);
                    extra -= 1;
                }
            },
            18 => {
                let (mut extra, size)= reader.read(7)?;
                debug_assert_eq!(size, 7);
                extra += 11;
                while extra > 0 {
                    lengths.push(0);
                    extra -= 1;
                }
            },
            _ => unreachable!(),
        }
    }
    Ok(lengths)
}

fn decode_non_compressed_compression<R:Read, W:Write>(reader:&mut R, output:&mut W) -> Result<(), DeflateError> {
    let mut buf = [0u8; 2];
    reader.read_exact(&mut buf)?;
    let len = u16::from_le_bytes(buf);
    let mut buf = [0u8; 2];
    reader.read_exact(&mut buf)?;
    let len_inverse = u16::from_le_bytes(buf);
    debug_assert_eq!(len, !len_inverse);
    let mut buf = vec![0u8; len as usize];
    reader.read_exact(&mut buf)?;
    output.write(&buf)?;
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;


    #[test]
    fn test_deflate_decode_fixed() -> Result<(), DeflateError>{
        let mut output = vec![];
        let mut input:&[u8] = &[0x33, 0x04, 0x00];
        deflate_decode(&mut input, &mut output)?;
        assert_eq!(output, "1".as_bytes());

        let mut output = vec![];
        let mut input:&[u8] = &[0x4b, 0x4c, 0x4a, 0x4e, 0x49, 0x4d, 0x4b, 0xcf, 0xc8, 0xcc, 0xca, 0xce, 0xc9, 0xcd, 0xcb, 0x2f, 0x50, 0x48, 0x44, 0x13, 0xd0, 0x29, 0x2c, 0x2a, 0x2e, 0x29, 0x2d, 0x2b, 0x07, 0x00];
        deflate_decode(&mut input, &mut output)?;
        assert_eq!(output, "abcdefghijklmnop abcdefghijklmnop,qrstuvw".as_bytes());

        let mut output = vec![];
        let mut input:&[u8] = &[0x0b, 0x4f, 0xcd, 0x49, 0xce, 0xcf, 0x4d, 0x55, 0x28, 0xc9, 0x57, 0xf0, 0x2d, 0xcd, 0x29, 0xc9, 0x2c, 0x2d, 0xc9, 0xcc, 0xd1, 0x53, 0x08, 0xc9, 0xc8, 0x2c, 0x56, 0x00, 0xa2, 0x92, 0x0c, 0x90, 0x4c, 0x7e, 0x8e, 0x42, 0x79, 0x46, 0x6a, 0x51, 0xaa, 0x42, 0x65, 0x7e, 0x29, 0x00];
        deflate_decode(&mut input, &mut output)?;
        assert_eq!(output, "Welcome to Multiutil. This is the tool where you".as_bytes());

        Ok(())
    }

    #[test]
    fn test_deflate_decode_dynamic() -> Result<(), DeflateError>{
        let mut output = vec![];
        let mut input:&[u8] = &[0x15, 0x8b, 0xc1, 0x09, 0x80, 0x40, 0x0c, 0x04, 0x5b, 0xd9, 0x0a, 0xec, 0xc4, 0x9f, 0xe0, 0x5b, 0x8e, 0x85, 0x04, 0xa2, 0x91, 0x24, 0x87, 0xda, 0xbd, 0x77, 0x30, 0xaf, 0x19, 0x66, 0xa7, 0x35, 0x3f, 0x89, 0x72, 0xac, 0xdd, 0x4a, 0x7b, 0xa9, 0x2d, 0xd8, 0x44, 0x13, 0x83, 0x92, 0x59, 0xdc, 0xf0, 0x08, 0x83, 0xf8, 0xbc, 0xa3, 0x1d, 0x17, 0xc6, 0x71, 0x07, 0x33, 0xa7, 0x08, 0x14, 0xdf, 0x82, 0xc7, 0x0f];
        deflate_decode(&mut input, &mut output)?;
        assert_eq!(output, "Welcome to Multiutil. This is the tool where you can compress your text or".as_bytes());
        Ok(())
    }

    #[test]
    fn test_deflate_decode_non_compressed() -> Result<(), DeflateError>{
        let mut output = vec![];
        let mut input:&[u8] = &[0x01, 0x01, 0x00, 0xfe, 0xff, 0x61];
        deflate_decode(&mut input, &mut output)?;
        assert_eq!(output, "a".as_bytes());
        Ok(())
    }
}