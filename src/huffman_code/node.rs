use std::fmt::{Formatter, Display};
use core::fmt;

///Node of huffman tree
/// `weight` - character frequency
/// `token_index` - token index in the alphabet, when equals to `usize::MAX` refers to intermediate node
/// `value` - serves as parent index during tree build step or final code length after calculation is done
pub struct Node {
    pub weight:usize,
    pub symbol_index:usize,
    pub parent_index_or_code_length:usize,
}

impl Node {
    pub fn new_leaf(symbol_index:usize, weight:usize) -> Self {
        Node {weight, symbol_index, parent_index_or_code_length:0}
    }

    pub fn new_node(weight:usize) -> Self {
        Node {weight, symbol_index:usize::MAX, parent_index_or_code_length:0}
    }

    pub fn is_leaf(&self) -> bool {
        self.symbol_index != usize::MAX
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let alpha = if self.symbol_index == usize::MAX {"N".to_string()} else {self.symbol_index.to_string()};
        write!(f, "({} {}, -> {})", alpha, self.weight, self.parent_index_or_code_length)
    }
}

impl fmt::Debug for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}