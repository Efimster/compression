use crate::huffman_code::node::Node;

enum WeightSum {
    LeafLeaf,
    LeafNode,
    NodeNode,
}

pub struct HuffmanEncoder {
    leaves:Vec<Node>,
    nodes:Vec<Node>,
    node_index:Option<usize>,
    leaf_index:usize,
}

impl HuffmanEncoder{
    pub fn new(alphabet_weights:&[usize]) -> Self {
        let mut leaves = alphabet_weights.iter()
            .cloned()
            .enumerate()
            .filter(|(_, weight)| *weight > 0)
            .map(|(index, weight)|Node::new_leaf(index, weight))
            .collect::<Vec<_>>();
        leaves.sort_unstable_by_key(|node| node.weight);
        HuffmanEncoder {
            leaves,
            nodes:Vec::with_capacity(alphabet_weights.len()),
            node_index:None,
            leaf_index: 0,
        }
    }

    pub fn into_code_lengths(mut self) -> Vec<usize>{
        if self.leaves.len() > 1 {
            let encoder = &mut self;
            while !encoder.root_found() {
                match encoder.min_weight_sum_path() {
                    WeightSum::LeafLeaf => encoder.leaf_leaf(),
                    WeightSum::LeafNode => encoder.leaf_node(),
                    WeightSum::NodeNode => encoder.node_node(),
                }
                // println!("leaves {:?}", &encoder.leaves[..]);
                // println!("nodes {:?}", &encoder.nodes);
            }
            encoder.indexes_to_lengths();
        }
        else if self.leaves.len() == 1{
            self.leaves[0].parent_index_or_code_length = 1;
        }
        else {
            return vec![];
        }
        let mut leaves = self.leaves;
        leaves.sort_unstable_by_key(|node|node.symbol_index);
        let mut result = vec![0usize; leaves[leaves.len() - 1].symbol_index + 1];
        for leaf in leaves {
            result[leaf.symbol_index] = leaf.parent_index_or_code_length;
        }
        // println!("result {:?}", &result);
        result
    }

    fn leaf_leaf(&mut self) {
        let nodes_len = self.nodes.len();
        let mut weight = self.leaf_weight().unwrap();
        self.set_leaf_parent(nodes_len);
        weight += self.leaf_weight().unwrap();
        self.set_leaf_parent(nodes_len);
        self.add_new_node(weight);
        if nodes_len == 0 {
            self.node_index = Some(0);
        }
    }

    fn leaf_node(&mut self){
        let nodes_len = self.nodes.len();
        let mut weight = self.leaf_weight().unwrap();
        self.set_leaf_parent(nodes_len);
        weight += self.node_weight().unwrap();
        self.set_node_parent(nodes_len);
        self.add_new_node(weight);
    }

    fn node_node(&mut self){
        let nodes_len = self.nodes.len();
        let mut weight = self.node_weight().unwrap();
        self.set_node_parent(nodes_len);
        weight += self.node_weight().unwrap();
        self.set_node_parent(nodes_len);
        self.add_new_node(weight);
    }

    fn leaf_weight(&self) -> Option<usize> {
        if self.leaf_index < self.leaves.len(){
            Some(self.leaves[self.leaf_index].weight)
        }
        else {
            None
        }
    }

    fn next_leaf_weight(&self) -> Option<usize> {
        if self.leaf_index + 1 < self.leaves.len(){
            Some(self.leaves[self.leaf_index + 1].weight)
        }
        else {
            None
        }
    }

    fn node_weight(&self) -> Option<usize> {
        match self.node_index {
            Some(index) if index < self.nodes.len() => Some(self.nodes[index].weight),
            _ => None,
        }
    }

    fn next_node_weight(&self) -> Option<usize> {
        match self.node_index {
            Some(index) if index + 1 < self.nodes.len() => Some(self.nodes[index + 1].weight),
            _ => None,
        }
    }

    fn set_leaf_parent(&mut self, parent_index:usize){
        let index = self.leaf_index;
        self.leaves[index].parent_index_or_code_length = parent_index;
        self.leaf_index += 1;
    }

    fn set_node_parent(&mut self, parent_index:usize){
        let index = self.node_index.take().unwrap();
        self.nodes[index].parent_index_or_code_length = parent_index;
        self.node_index = Some(index + 1);
    }

    fn add_new_node(&mut self, weight:usize){
        let node = Node::new_node(weight);
        self.nodes.push(node);
    }

    fn root_found(&self) -> bool{
        if self.leaf_index < self.leaves.len() {
            return false;
        }

        match self.node_index {
            None => self.leaves.len() == 0,
            Some(index) => index == self.nodes.len() - 1,
        }
    }

    fn min_weight_sum_path(&self) -> WeightSum {
       debug_assert!(!self.root_found());
        if let None = self.leaf_weight() {
            return WeightSum::NodeNode;
        }

        if let None = self.node_weight() {
            return WeightSum::LeafLeaf;
        }
        let leaf_weight = self.leaf_weight().unwrap();
        let node_weight = self.node_weight().unwrap();
        if leaf_weight <= node_weight{
            match self.next_leaf_weight() {
                Some(weight) if weight <= node_weight => WeightSum::LeafLeaf,
                _ => WeightSum::LeafNode,
            }
        }
        else {
            match self.next_node_weight() {
                Some(weight) if weight > leaf_weight => WeightSum::NodeNode,
                _ => WeightSum::LeafNode,
            }
        }
    }

    fn indexes_to_lengths(&mut self){
        let nodes_len = self.nodes.len();
        for i in (0 .. nodes_len - 1).rev() {
            let index = self.nodes[i].parent_index_or_code_length;
            let value = self.nodes[index].parent_index_or_code_length + 1;
            self.nodes[i].parent_index_or_code_length = value;
        }

        for i in 0 .. self.leaves.len(){
            let leaf = &mut self.leaves[i];
            leaf.parent_index_or_code_length = 1 + self.nodes[leaf.parent_index_or_code_length].parent_index_or_code_length;
        }
        // println!("==================================");
        // println!("final leaves {:?}", &self.leaves);
        // println!("final nodes {:?}", &self.nodes);
    }
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_calculation_lengths(){
        let encoder = HuffmanEncoder::new(&[17, 4, 8, 3]);
        assert_eq!(&encoder.into_code_lengths(), &[1, 3, 2, 3]);
        let encoder = HuffmanEncoder::new(&[25, 25, 20, 15, 10, 5]);
        assert_eq!(&encoder.into_code_lengths(), &[2, 2, 2, 3, 4, 4]);
    }

}
