use crate::zip::utils::central_directory_util::{locate_central_directory, read_central_directory, central_vs_local_file_headers};
use std::io::{Seek, Read, SeekFrom};
use crate::zip::enums::zip_error::{ZipError, AnotherDiskRequestReason};
use crate::zip::structs::central_directory_header::CentralDirectoryHeader;
use crate::zip::structs::local_file_header::LocalFileHeader;
use crate::zip::enums::compression_method::CompressionMethod;
use crate::deflate::decode::deflate_decode;

use super::enums::zip_error::ZipErrorKind;

pub fn read_central_directory_headers<S:Read + Seek + ?Sized>(input:&mut S) -> Result<Vec<CentralDirectoryHeader>, ZipError> {
    let eocd = locate_central_directory(input)?;
    if eocd.disk_number != eocd.cd_start_disk_number {
        return Err(ZipErrorKind::AnotherDiskRequired(AnotherDiskRequestReason::CentralDirectoryStart(eocd)).into());
    }
    input.seek(SeekFrom::Start(eocd.cd_offset))?;
    let cd = read_central_directory(input, eocd.disk_cd_entries_total_number)?;
    Ok(cd)
}

pub fn read_file<S>(input:&mut S, cd_header:&CentralDirectoryHeader) -> Result<Vec<u8>, ZipError>
 where S:Read + Seek + ?Sized,
{
    input.seek(SeekFrom::Start(cd_header.local_header_offset))?;
    let local_file_header = LocalFileHeader::decode(input)?;
    central_vs_local_file_headers(cd_header, &local_file_header)?;
    if cd_header.encrypted() {
        return Err(ZipErrorKind::UnsupportedEncryption.into());
    }
    if cd_header.compression_method != CompressionMethod::Deflate{
        return Err(ZipErrorKind::UnsupportedCompressionMethod(cd_header.compression_method).into());
    }
    let mut buf = Vec::new();
    deflate_decode(input, &mut buf)?;
    Ok(buf)
}