use std::io::Read;
use crate::zip::enums::zip_error::ZipError;

pub fn decode_string<R:Read + ?Sized>(reader:&mut R, length:usize, utf8:bool) -> Result<String, ZipError>{
    match decode_optional_bytes(reader, length)? {
        None => Ok(String::new()),
        Some(data) => {
            let string = if utf8 {
                String::from_utf8(data)?
            }
            else {
                latin1_to_string(&data)
            };
            Ok(string)
        },
    }
}

pub fn decode_optional_bytes<R:Read + ?Sized>(reader:&mut R, length:usize) -> Result<Option<Vec<u8>>, ZipError>{
    if length == 0 {
        return Ok(None);
    }
    let mut data = vec![0u8; length];
    reader.read_exact(&mut data)?;
    Ok(Some(data))
}

pub fn latin1_to_string(s: &[u8]) -> String {
    s.iter().map(|&c| c as char).collect()
}