use std::io::{Seek, Read, SeekFrom};
use crate::zip::structs::end_of_central_directory_record::EndOfCentralDirectoryRecord;
use crate::zip::enums::zip_error::{AnotherDiskRequestReason, ZipError, ZipErrorKind};
use crate::zip::constants::{RECORD_SIGNATURE_LENGTH, END_OF_CENTRAL_DIR_SIGNATURE, ZIP64_END_OF_CENTRAL_DIR_LOCATOR};
use crate::zip::structs::zip64_end_of_central_directory_record::Zip64EndOfCentralDirectoryLocator;
use crate::zip::structs::central_directory_header::CentralDirectoryHeader;
use crate::zip::structs::local_file_header::LocalFileHeader;


pub fn locate_central_directory<S:Seek + Read + ?Sized>(reader:&mut S) -> Result<EndOfCentralDirectoryRecord, ZipError>{
    let mut buf = [0u8; RECORD_SIGNATURE_LENGTH];
    let position = locate_end_of_central_directory_signature(reader, &mut buf)?;
    reader.seek(SeekFrom::Start(position))?;
    let mut v = vec![];
    reader.read_to_end(&mut v)?;
    reader.seek(SeekFrom::Start(position))?;
    let eocd_record = EndOfCentralDirectoryRecord::decode(reader)?;
    reader.seek(SeekFrom::Start(position - 20))?;
    reader.read_exact(&mut buf)?;
    if buf == ZIP64_END_OF_CENTRAL_DIR_LOCATOR {
        reader.seek(SeekFrom::Start(position - 20))?;
        let locator = Zip64EndOfCentralDirectoryLocator::decode(reader)?;
        if locator.zip64_eocd_disk_number != eocd_record.disk_number {
            return Err(ZipErrorKind::AnotherDiskRequired(AnotherDiskRequestReason::Zip64EndOfCentralDirectory(locator)).into());
        }
        let mut zip_64_eocd_record = EndOfCentralDirectoryRecord::decode_zip64(reader)?;
        zip_64_eocd_record.comment = eocd_record.comment;
        return Ok(zip_64_eocd_record);
    }
    Ok(eocd_record)
}

pub fn locate_end_of_central_directory_signature<S:Seek + Read + ?Sized>(reader:&mut S, buf:&mut [u8; RECORD_SIGNATURE_LENGTH]) -> Result<u64, ZipError>{
    let mut position = reader.seek(SeekFrom::End(-22))?;//22 - least possible end offset
    reader.read_exact(buf)?;
    loop {
        let mut compare_len = 0usize;
        while compare_len < RECORD_SIGNATURE_LENGTH && buf[0] != END_OF_CENTRAL_DIR_SIGNATURE[compare_len] {
            compare_len += 1;
        }

        if compare_len == 0 {
            return Ok(position);
        }
        buf.copy_within( .. RECORD_SIGNATURE_LENGTH - compare_len, compare_len);
        position = reader.seek(SeekFrom::Start(position - compare_len as u64))?;
        reader.read_exact(&mut buf[..compare_len])?;
    }
}

pub fn read_central_directory<S:Read + ?Sized>(reader:&mut S, count:u64) -> Result<Vec<CentralDirectoryHeader>, ZipError>{
    let mut cd_headers = Vec::with_capacity(count as usize);
    for _ in 0 .. count {
        cd_headers.push(CentralDirectoryHeader::decode(reader)?);
    }
    Ok(cd_headers)
}

pub fn central_vs_local_file_headers(cd_header:&CentralDirectoryHeader, local_file_header:&LocalFileHeader) -> Result<(), ZipError> {
    if cd_header.version_required != local_file_header.version_required
        || cd_header.compression_method != local_file_header.compression_method
        || cd_header.modification_date != local_file_header.modification_date
        || cd_header.file_name != local_file_header.file_name
        || cd_header.bit_flag != local_file_header.bit_flag
    {
        return Err(ZipErrorKind::CentralDirectoryHeaderNotMatchingLocalFileHeader.into())
    }

    if bit_flag_masked(local_file_header.bit_flag) {
        if cd_header.crc32 != local_file_header.crc32
            || cd_header.compressed_size != local_file_header.compressed_size
            || cd_header.uncompressed_size != local_file_header.uncompressed_size
        {
            return Err(ZipErrorKind::CentralDirectoryHeaderNotMatchingLocalFileHeader.into())
        }
    }
    Ok(())
}

#[inline]
pub fn bit_flag_masked(bit_flag:u16) -> bool {
    bit_flag & 0b1000 > 0
}
#[inline]
pub fn bit_flag_encrypted(bit_flag:u16) -> bool {
    bit_flag & 0b01 > 0
}
#[inline]
pub fn bit_flag_utf8_file_name(bit_flag:u16) -> bool {
    bit_flag & 0x0800 > 0
}

#[cfg(test)]
mod test {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn test_locate_end_of_central_directory_signature() -> Result<(), ZipError>{
        let mut input = vec![5u8; 23];
        input.extend_from_slice(&END_OF_CENTRAL_DIR_SIGNATURE);
        input.extend_from_slice(&[7u8; 27]);
        let mut input = Cursor::new(input);
        let mut buf = [0u8; RECORD_SIGNATURE_LENGTH];
        assert_eq!(locate_end_of_central_directory_signature(&mut input, &mut buf)?, 23);
        Ok(())
    }
}