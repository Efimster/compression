use crate::zip::enums::compression_method::CompressionMethod;
use date::date_time::DateTime;
use std::io::Read;
use crate::zip::enums::zip_error::ZipError;
use crate::zip::constants::{RECORD_SIGNATURE_LENGTH, LOCAL_FILE_HEADER_SIGNATURE};
use std::convert::TryInto;
use date::date::Date;
use date::time::Time;
use crate::zip::utils::{decode_util, central_directory_util};
use crate::zip::enums::extensible_data::ExtensibleData;
use std::collections::HashMap;
use crate::zip::enums::extensible_data;
use core::fmt;
use std::fmt::{Display, Formatter};

pub struct LocalFileHeader{
    pub version_required:u16,
    pub bit_flag:u16,
    pub compression_method:CompressionMethod,
    pub modification_date:DateTime,
    pub crc32:u32,
    pub compressed_size:u64,
    pub uncompressed_size:u64,
    pub file_name:String,
    pub extra_field:HashMap<u16, ExtensibleData>,
    pub file_name_length:usize,
    pub extra_field_length:usize,
}

impl LocalFileHeader {
    #[inline]
    pub fn masked(&self) -> bool {
        central_directory_util::bit_flag_masked(self.bit_flag)
    }
    #[inline]
    pub fn encrypted(&self) -> bool {
        central_directory_util::bit_flag_encrypted(self.bit_flag)
    }
    #[inline]
    pub fn utf8_file_name(&self) -> bool {
        central_directory_util::bit_flag_utf8_file_name(self.bit_flag)
    }

    pub fn decode<R:Read + ?Sized>(reader:&mut R) -> Result<Self, ZipError>{
        let mut buf = [0u8; 30];
        reader.read_exact(&mut buf)?;
        debug_assert_eq!(&buf[..RECORD_SIGNATURE_LENGTH], &LOCAL_FILE_HEADER_SIGNATURE);
        let version_required = u16::from_le_bytes((&buf[4 .. 6]).try_into()?);
        let bit_flag = u16::from_le_bytes((&buf[6 .. 8]).try_into()?);
        let compression_method = CompressionMethod::decode(&mut &buf[8 .. 10])?;
        let last_modification_file_time = u16::from_le_bytes((&buf[10 .. 12]).try_into()?);
        let last_modification_file_date = u16::from_le_bytes((&buf[12 .. 14]).try_into()?);
        let modification_date = DateTime::new(Date::from_ms_dos_date(last_modification_file_date),
              Time::from_ms_dos_time(last_modification_file_time), 0);
        let crc32 = u32::from_le_bytes((&buf[14 .. 18]).try_into()?);
        let mut compressed_size = u32::from_le_bytes((&buf[18 .. 22]).try_into()?) as u64;
        let mut uncompressed_size = u32::from_le_bytes((&buf[22 .. 26]).try_into()?) as u64;
        let file_name_length = u16::from_le_bytes((&buf[26 .. 28]).try_into()?) as usize;
        let extra_field_length = u16::from_le_bytes((&buf[28 .. 30]).try_into()?) as usize;
        let file_name = decode_util::decode_string(reader, file_name_length,
               central_directory_util::bit_flag_utf8_file_name(bit_flag))?;
        let extra_field = decode_util::decode_optional_bytes(reader, extra_field_length)?;
        let mut extra_field = match extra_field {
            Some(extra_field) => ExtensibleData::decode_list(&extra_field)?,
            None => HashMap::new(),
        };
        let zip64_extended= extra_field.remove(&extensible_data::ZIP_64_EXTENDED);
        let masked = central_directory_util::bit_flag_masked(bit_flag) && crc32 == 0 && compressed_size == 0 && uncompressed_size == 0;
        match zip64_extended {
            Some(ExtensibleData::Zip64Extended(data)) => {
                let mut start = 0usize;
                if uncompressed_size as u32 == u32::MAX || masked {
                    uncompressed_size = u64::from_le_bytes((&data[start .. start + 8]).try_into()?);
                    start += 8;
                }
                if compressed_size as u32 == u32::MAX || masked{
                    compressed_size = u64::from_le_bytes((&data[start .. start + 8]).try_into()?);
                }
            },
            _ => (),
        }

        let result = LocalFileHeader {
            version_required,
            bit_flag,
            compression_method,
            modification_date,
            crc32,
            compressed_size,
            uncompressed_size,
            file_name,
            extra_field,
            file_name_length,
            extra_field_length,
        };
        Ok(result)
    }

}

impl fmt::Display for LocalFileHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let file_name = if self.file_name.len() > 0 {&self.file_name} else {"__NO_FILE_NAME__"};
        write!(f, "{} | Compression:{:?} | Modification Date:{} | Compressed size:{} | Uncompressed size:{} | Ratio:{}%",
               file_name, self.compression_method, self.modification_date,
               self.compressed_size, self.uncompressed_size,
               (self.compressed_size as f64 / self.uncompressed_size as f64 * 100f64) as usize)
    }
}

impl fmt::Debug for LocalFileHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

