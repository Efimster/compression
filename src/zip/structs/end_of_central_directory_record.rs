use std::io::Read;
use crate::zip::enums::zip_error::ZipError;
use std::convert::TryInto;
use crate::zip::constants::{END_OF_CENTRAL_DIR_SIGNATURE, RECORD_SIGNATURE_LENGTH, ZIP64_END_OF_CENTRAL_DIR_SIGNATURE};
use crate::zip::utils::decode_util;


///`disk_number` - number of this disk
/// `cd_start_disk_number` - number of the disk with the start of the central directory
/// `disk_cd_entries_total_number` - total number of entries in the central directory on this disk
/// `cd_entries_total_number` - total number of entries in the central directory
/// `cd_size` - size of the central directory
/// `cd_offset` - offset of start of central directory with respect to the starting disk number
#[derive(Debug, Clone)]
pub struct EndOfCentralDirectoryRecord {
    pub version_made_by:usize,
    pub version_required:usize,
    pub disk_number:usize,
    pub cd_start_disk_number:usize,
    pub disk_cd_entries_total_number:u64,
    pub cd_entries_total_number:u64,
    pub cd_size:u64,
    pub cd_offset:u64,
    pub comment:String,
    pub zip_64_extensible_data:Option<Vec<u8>>,
}

impl EndOfCentralDirectoryRecord {
    pub fn decode<R:Read + ?Sized>(reader:&mut R) -> Result<Self, ZipError>{
        let mut buf = [0u8; 22];
        reader.read_exact(&mut buf)?;
        debug_assert_eq!(&buf[..RECORD_SIGNATURE_LENGTH], &END_OF_CENTRAL_DIR_SIGNATURE);
        let disk_number = u16::from_le_bytes((&buf[4 .. 6]).try_into()?) as usize;
        let cd_start_disk_number = u16::from_le_bytes((&buf[6 .. 8]).try_into()?) as usize;
        let disk_cd_entries_total_number = u16::from_le_bytes((&buf[8 .. 10]).try_into()?) as u64;
        let cd_entries_total_number = u16::from_le_bytes((&buf[10 .. 12]).try_into()?) as u64;
        let cd_size = u32::from_le_bytes((&buf[12 .. 16]).try_into()?) as u64;
        let cd_offset = u32::from_le_bytes((&buf[16 .. 20]).try_into()?) as u64;
        let comment_len = u16::from_le_bytes((&buf[20 .. 22]).try_into()?) as usize;
        let comment =  decode_util::decode_string(reader, comment_len, false)?;

        let result = EndOfCentralDirectoryRecord {
            version_made_by: 1,
            version_required: 1,
            disk_number,
            cd_start_disk_number,
            disk_cd_entries_total_number,
            cd_entries_total_number,
            cd_size,
            cd_offset,
            comment,
            zip_64_extensible_data:None,
        };
        Ok(result)
    }

    pub fn decode_zip64<R:Read + ?Sized>(reader:&mut R) -> Result<Self, ZipError>{
        let mut buf = [0u8; 56];
        reader.read_exact(&mut buf)?;
        debug_assert_eq!(&buf[..RECORD_SIGNATURE_LENGTH], &ZIP64_END_OF_CENTRAL_DIR_SIGNATURE);
        let size = u64::from_le_bytes((&buf[4 .. 12]).try_into()?) as usize;
        let version_made_by = u16::from_le_bytes((&buf[12 .. 14]).try_into()?) as usize;
        let version_required = u16::from_le_bytes((&buf[14 .. 16]).try_into()?) as usize;
        let disk_number = u32::from_le_bytes((&buf[16 .. 20]).try_into()?) as usize;
        let cd_start_disk_number = u32::from_le_bytes((&buf[20 .. 24]).try_into()?) as usize;
        let disk_cd_entries_total_number = u64::from_le_bytes((&buf[24 .. 32]).try_into()?);
        let cd_entries_total_number = u64::from_le_bytes((&buf[32 .. 40]).try_into()?);
        let cd_size = u64::from_le_bytes((&buf[40 .. 48]).try_into()?);
        let cd_offset = u64::from_le_bytes((&buf[48 .. 56]).try_into()?);
        let zip_64_extensible_data = decode_util::decode_optional_bytes(reader, size)?;

        let result = EndOfCentralDirectoryRecord {
            version_made_by,
            version_required,
            disk_number,
            cd_start_disk_number,
            disk_cd_entries_total_number,
            cd_entries_total_number,
            cd_size,
            cd_offset,
            comment:String::new(),
            zip_64_extensible_data,
        };
        Ok(result)
    }
}