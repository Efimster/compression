use crate::zip::enums::compression_method::CompressionMethod;
use date::date_time::DateTime;
use std::io::Read;
use crate::zip::enums::zip_error::ZipError;
use crate::zip::constants::{RECORD_SIGNATURE_LENGTH, CENTRAL_FILE_HEADER_SIGNATURE};
use std::convert::TryInto;
use date::date::Date;
use date::time::Time;
use crate::zip::utils::{decode_util, central_directory_util};
use crate::zip::enums::extensible_data::ExtensibleData;
use std::collections::HashMap;
use crate::zip::enums::extensible_data;
use core::fmt;
use std::fmt::{Display, Formatter};

pub struct CentralDirectoryHeader{
    pub version_made_by:u16,
    pub version_required:u16,
    pub bit_flag:u16,
    pub compression_method:CompressionMethod,
    pub modification_date:DateTime,
    pub crc32:u32,
    pub compressed_size:u64,
    pub uncompressed_size:u64,
    pub disk_number_start:usize,
    pub local_header_offset:u64,
    pub internal_file_attributes:u16,
    pub external_file_attributes:u32,
    pub file_name:String,
    pub file_comment:String,
    pub extra_field:HashMap<u16, ExtensibleData>,
}

impl CentralDirectoryHeader {
    #[inline]
    pub fn masked(&self) -> bool {
        central_directory_util::bit_flag_masked(self.bit_flag)
    }
    #[inline]
    pub fn encrypted(&self) -> bool {
        central_directory_util::bit_flag_encrypted(self.bit_flag)
    }
    #[inline]
    pub fn utf8_file_name(&self) -> bool {
        central_directory_util::bit_flag_utf8_file_name(self.bit_flag)
    }

    pub fn marked_as_textual(&self) -> bool {
        self.internal_file_attributes & 0x1 > 0
    }

    pub fn decode<R:Read + ?Sized>(reader:&mut R) -> Result<Self, ZipError>{
        let mut buf = [0u8; 46];
        reader.read_exact(&mut buf)?;
        debug_assert_eq!(&buf[..RECORD_SIGNATURE_LENGTH], &CENTRAL_FILE_HEADER_SIGNATURE);

        let version_made_by = u16::from_le_bytes((&buf[4 .. 6]).try_into()?);
        let version_required = u16::from_le_bytes((&buf[6 .. 8]).try_into()?);
        let bit_flag = u16::from_le_bytes((&buf[8 .. 10]).try_into()?);
        let compression_method = CompressionMethod::decode(&mut &buf[10 .. 12])?;
        let last_modification_file_time = u16::from_le_bytes((&buf[12 .. 14]).try_into()?);
        let last_modification_file_date = u16::from_le_bytes((&buf[14 .. 16]).try_into()?);
        let modification_date = DateTime::new(Date::from_ms_dos_date(last_modification_file_date),
              Time::from_ms_dos_time(last_modification_file_time), 0);
        let crc32 = u32::from_le_bytes((&buf[16 .. 20]).try_into()?);
        let mut compressed_size = u32::from_le_bytes((&buf[20 .. 24]).try_into()?) as u64;
        let mut uncompressed_size = u32::from_le_bytes((&buf[24 .. 28]).try_into()?) as u64;
        let file_name_length = u16::from_le_bytes((&buf[28 .. 30]).try_into()?) as usize;
        let extra_field_length = u16::from_le_bytes((&buf[30 .. 32]).try_into()?) as usize;
        let file_comment_length = u16::from_le_bytes((&buf[32 .. 34]).try_into()?) as usize;
        let mut disk_number_start = u16::from_le_bytes((&buf[34 .. 36]).try_into()?) as usize;
        let internal_file_attributes = u16::from_le_bytes((&buf[36 .. 38]).try_into()?);
        let external_file_attributes = u32::from_le_bytes((&buf[38 .. 42]).try_into()?);
        let mut local_header_offset = u32::from_le_bytes((&buf[42 .. 46]).try_into()?) as u64;
        let file_name = decode_util::decode_string(reader, file_name_length,
               central_directory_util::bit_flag_utf8_file_name(bit_flag))?;
        let extra_field = decode_util::decode_optional_bytes(reader, extra_field_length)?;
        let file_comment = decode_util::decode_string(reader, file_comment_length,
                  central_directory_util::bit_flag_utf8_file_name(bit_flag))?;
        let mut extra_field = match extra_field {
            Some(extra_field) => ExtensibleData::decode_list(&extra_field)?,
            None => HashMap::new(),
        };
        let zip64_extended= extra_field.remove(&extensible_data::ZIP_64_EXTENDED);
        match zip64_extended {
            Some(ExtensibleData::Zip64Extended(data)) => {
                let mut start = 0usize;
                if uncompressed_size as u32 == u32::MAX {
                    uncompressed_size = u64::from_le_bytes((&data[start .. start + 8]).try_into()?);
                    start += 8;
                }
                if compressed_size as u32 == u32::MAX {
                    compressed_size = u64::from_le_bytes((&data[start .. start + 8]).try_into()?);
                    start += 8;
                }
                if local_header_offset as u32 == u32::MAX {
                    local_header_offset = u64::from_le_bytes((&data[start .. start + 8]).try_into()?);
                    start += 8;
                }
                if disk_number_start as u16 == u16::MAX {
                    disk_number_start = u32::from_le_bytes((&data[start .. start + 4]).try_into()?) as usize;
                }
            },
            _ => (),
        }

        // this does not work as provides weired results
        // let extended_timestamp= extra_field.remove(&extensible_data::EXTENDED_TIMESTAMP);
        // match extended_timestamp {
        //     Some(ExtensibleData::ExtendedTimestamp(data)) => {
        //         let seconds = u32::from_le_bytes((&buf[1 .. 5]).try_into()?);
        //         dbg!(data.len(), buf[0], seconds);
        //         modification_date = DateTime::from_seconds_since_unix_epoch(seconds as u64);
        //     },
        //     _ => (),
        // }


        let result = CentralDirectoryHeader {
            version_made_by,
            version_required,
            bit_flag,
            compression_method,
            modification_date,
            crc32,
            compressed_size,
            uncompressed_size,
            disk_number_start,
            internal_file_attributes,
            external_file_attributes,
            local_header_offset,
            file_name,
            file_comment,
            extra_field,
        };
        Ok(result)
    }

}

impl fmt::Display for CentralDirectoryHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let file_name = if self.file_name.len() > 0 {&self.file_name} else {"__NO_FILE_NAME__"};
        let text_attribute = if self.marked_as_textual() {" (Text) "} else {""};
        write!(f, "{}{} | Compression:{:?} | Modification Date:{} | Compressed size:{} | Uncompressed size:{} | Ratio:{}%",
               file_name, text_attribute, self.compression_method, self.modification_date,
               self.compressed_size, self.uncompressed_size,
               (self.compressed_size as f64 / self.uncompressed_size as f64 * 100f64) as usize)
    }
}

impl fmt::Debug for CentralDirectoryHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

