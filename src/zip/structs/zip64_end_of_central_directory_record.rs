use std::io::Read;
use crate::zip::enums::zip_error::ZipError;
use crate::zip::constants::{RECORD_SIGNATURE_LENGTH, ZIP64_END_OF_CENTRAL_DIR_LOCATOR};
use std::convert::TryInto;

/// `zip64_eocd_disk_number` - number of the disk with the start of the zip64 end of central directory
/// `zip64_eocd_offset` - relative offset of the zip64 end of central directory record
/// `total_disks` - total number of disks
#[derive(Debug, Clone, Copy)]
pub struct Zip64EndOfCentralDirectoryLocator {
    pub zip64_eocd_disk_number:usize,
    pub zip64_eocd_offset:u64,
    pub total_disks:usize,
}

impl Zip64EndOfCentralDirectoryLocator {
    pub fn decode<R:Read + ?Sized>(reader:&mut R) -> Result<Self, ZipError>{
        let mut buf = [0u8; 20];
        reader.read_exact(&mut buf)?;
        debug_assert_eq!(&buf[..RECORD_SIGNATURE_LENGTH], &ZIP64_END_OF_CENTRAL_DIR_LOCATOR);
        let zip64_eocd_disk_number = u32::from_le_bytes((&buf[4 .. 8]).try_into()?) as usize;
        let zip64_eocd_offset = u64::from_le_bytes((&buf[8 .. 16]).try_into()?);
        let total_disks = u32::from_le_bytes((&buf[16 .. ]).try_into()?) as usize;
        Ok(Zip64EndOfCentralDirectoryLocator {
            zip64_eocd_disk_number,
            zip64_eocd_offset,
            total_disks,
        })
    }
}