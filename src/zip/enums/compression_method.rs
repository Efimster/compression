use std::io::Read;
use crate::zip::enums::zip_error::ZipError;
use crate::zip::enums::compression_method::CompressionMethod::Unknown;

pub const STORE:u16 = 0;
const UN_SHRINKING:u16 = 1;
const EXPANDING_1:u16 = 2;
const EXPANDING_2:u16 = 3;
const EXPANDING_3:u16 = 4;
const EXPANDING_4:u16 = 5;
const IMPLODING:u16 = 6;
const TOKENIZING:u16 = 7;
pub const DEFLATING:u16 = 8;
const DEFLATING_64:u16 = 9;
const PKWARE_IMPLODING:u16 = 10;
const RESERVED_11:u16 = 11;
const BZIP2:u16 = 12;
const RESERVED_13:u16 = 13;
const LZMA:u16 = 14;
const RESERVED_15:u16 = 15;
const IBM_Z_OS_CMPSC:u16 = 16;
const RESERVED_17:u16 = 17;
const IBM_TERSE:u16 = 18;
const IBM_LZ77_Z_ARCH:u16 = 19;
const ZSTD:u16 = 93;
const MP3:u16 = 94;
const XZ:u16 = 95;
const JPEG_VARIANT:u16 = 96;
const WAV_PACK:u16 = 97;
const PPMD:u16 = 98;
const AE_X_ENCRYPTION_MARKER:u16 = 99;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum CompressionMethod {
    Store,
    UnShrinking,
    Expanding(u16),
    Imploding,
    Tokenizing,
    Deflate,
    Deflate64,
    BZip2,
    Lzma,
    WavPack,
    Ppmd,
    AExEncryptionMarker,
    JpegVariant,
    PkWareImploding,
    Reserved(u16),
    Mp3,
    Xz,
    IbmZOsCmpsc,
    IbmTerse,
    IbmLz77,
    Unknown(u16),
}

impl CompressionMethod {
    pub fn decode<R:Read>(reader:&mut R) -> Result<CompressionMethod, ZipError> {
        let mut value = [0u8; 2];
        reader.read_exact(&mut value)?;
        let value = u16::from_le_bytes(value);
        let value = match value {
            STORE => CompressionMethod::Store,
            UN_SHRINKING => CompressionMethod::UnShrinking,
            EXPANDING_1 | EXPANDING_2 | EXPANDING_3 | EXPANDING_4 => CompressionMethod::Expanding(value),
            IMPLODING => CompressionMethod::Imploding,
            TOKENIZING => CompressionMethod::Tokenizing,
            DEFLATING => CompressionMethod::Deflate,
            DEFLATING_64 => CompressionMethod::Deflate64,
            PKWARE_IMPLODING => CompressionMethod::PkWareImploding,
            BZIP2 => CompressionMethod::BZip2,
            LZMA => CompressionMethod::Lzma,
            IBM_Z_OS_CMPSC => CompressionMethod::IbmZOsCmpsc,
            IBM_TERSE => CompressionMethod::IbmTerse,
            IBM_LZ77_Z_ARCH => CompressionMethod::IbmLz77,
            ZSTD => CompressionMethod::Store,
            MP3 => CompressionMethod::Mp3,
            XZ => CompressionMethod::Xz,
            JPEG_VARIANT => CompressionMethod::JpegVariant,
            WAV_PACK => CompressionMethod::WavPack,
            PPMD => CompressionMethod::Ppmd,
            AE_X_ENCRYPTION_MARKER => CompressionMethod::AExEncryptionMarker,
            RESERVED_11 | RESERVED_13 | RESERVED_15 | RESERVED_17 => CompressionMethod::Reserved(value),
            _ => Unknown(value),
        };
        Ok(value)
    }
}

