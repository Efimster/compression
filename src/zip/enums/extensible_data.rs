use std::convert::TryInto;
use crate::zip::enums::zip_error::ZipError;
use std::collections::HashMap;

pub const ZIP_64_EXTENDED:u16 = 0x0001;
const NTFS:u16 = 0x000a;
const UNIX:u16 = 0x000d;
pub const EXTENDED_TIMESTAMP:u16 = 0x5455;
pub const NEW_UNIX:u16 = 0x7875;

#[derive(Debug)]
pub enum ExtensibleData {
    Zip64Extended(Box<[u8]>),
    Ntfs(Box<[u8]>),
    Unix(Box<[u8]>),
    ExtendedTimestamp(Box<[u8]>),
    NewUnix(Box<[u8]>),
    Other(u16, Box<[u8]>),
}

impl ExtensibleData {
    pub fn decode(buf:&[u8]) -> Result<(ExtensibleData, u16, usize), ZipError> {
        let header_id = u16::from_le_bytes((&buf[ .. 2]).try_into()?);
        let end = u16::from_le_bytes((&buf[2 .. 4]).try_into()?) as usize + 4;
        let data:Box<[u8]> = Box::from(&buf[4 .. end]);
        let result = match header_id {
            ZIP_64_EXTENDED => ExtensibleData::Zip64Extended(data),
            NTFS => ExtensibleData::Ntfs(data),
            UNIX => ExtensibleData::Unix(data),
            NEW_UNIX => ExtensibleData::NewUnix(data),
            EXTENDED_TIMESTAMP => ExtensibleData::ExtendedTimestamp(data),
            _ => ExtensibleData::Other(header_id, data),
        };
        Ok((result, header_id, end))
    }

    pub fn decode_list(mut bytes:&[u8]) -> Result<HashMap<u16, ExtensibleData>, ZipError> {
        let mut result = HashMap::new();
        while bytes.len() > 0 {
            let (extensible_data, header_id, size) =  Self::decode(bytes)?;
            result.insert(header_id, extensible_data);
            bytes = &bytes[size ..];
        }
        Ok(result)
    }
}