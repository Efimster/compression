use core::fmt;
use std::error::Error;
use std::io;
use std::str::Utf8Error;
use std::array::TryFromSliceError;
use std::string::FromUtf8Error;
use crate::zip::structs::zip64_end_of_central_directory_record::Zip64EndOfCentralDirectoryLocator;
use crate::zip::structs::end_of_central_directory_record::EndOfCentralDirectoryRecord;
use crate::zip::enums::compression_method::CompressionMethod;
use crate::deflate::enums::deflate_error::DeflateError;

#[derive(Debug, Clone)]
pub enum ZipErrorKind {
    EndOfCentralDirectoryRecordNotFound,
    AnotherDiskRequired(AnotherDiskRequestReason),
    CentralDirectoryHeaderNotMatchingLocalFileHeader,
    UnsupportedEncryption,
    UnsupportedCompressionMethod(CompressionMethod),
    Inner
}

#[derive(Debug, Clone)]
pub enum AnotherDiskRequestReason {
    Zip64EndOfCentralDirectory(Zip64EndOfCentralDirectoryLocator),
    CentralDirectoryStart(EndOfCentralDirectoryRecord),
}

#[derive(Debug)]
pub struct ZipError {
    pub source:Option<Box<dyn Error>>,
    pub kind:ZipErrorKind,
}

impl ZipError {
    pub fn new_inner(source:Box<dyn Error>)->Self{
        ZipError {
            source: Some(source),
            kind: ZipErrorKind::Inner
        }
    }

    pub fn new_kind(kind:ZipErrorKind)->Self{
        ZipError {
            source: None,
            kind
        }
    }
}

impl Error for ZipError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.source.as_deref()
    }
}

impl ZipErrorKind {
    pub fn into_boxed_error(self) -> Box<dyn Error> {
        let error:ZipError = self.into();
        error.into()
    }
}

impl fmt::Display for ZipErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ZipErrorKind::EndOfCentralDirectoryRecordNotFound => 
                write!(f, "the record of central dictionary 'end' not found"),
            ZipErrorKind::AnotherDiskRequired(reason) => 
                write!(f, "another disk required ({})", reason),
            ZipErrorKind::CentralDirectoryHeaderNotMatchingLocalFileHeader => 
                write!(f, "central dictionary header should match with local file header"),
            ZipErrorKind::UnsupportedEncryption => 
                write!(f, "unsupported encryption"),
            ZipErrorKind::UnsupportedCompressionMethod(method) => 
                write!(f, "unsupported compression method {:?}", method),
            ZipErrorKind::Inner =>  write!(f, "error in ZipError::source"),
        }
    }
}

impl fmt::Display for AnotherDiskRequestReason {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AnotherDiskRequestReason::CentralDirectoryStart(_) => write!(f, "end of central dictionary"),
            AnotherDiskRequestReason::Zip64EndOfCentralDirectory(locator) => 
                write!(f, "disk {} out of {} is missing", locator.zip64_eocd_disk_number, locator.total_disks),
        }
    }
}

impl fmt::Display for ZipError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl From<ZipErrorKind> for ZipError {
    fn from(kind: ZipErrorKind) -> Self {
        Self::new_kind(kind)
    }
}

impl From<Box<dyn Error>> for ZipError {
    fn from(error: Box<dyn Error>) -> Self {
        Self::new_inner(error)
    }
}

impl From<io::Error> for ZipError {
    fn from(error: io::Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<TryFromSliceError> for ZipError {
    fn from(error: TryFromSliceError) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<Utf8Error> for ZipError {
    fn from(error: Utf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<FromUtf8Error> for ZipError {
    fn from(error: FromUtf8Error) -> Self {
        Self::new_inner(error.into())
    }
}

impl From<DeflateError> for ZipError {
    fn from(error: DeflateError) -> Self {
        Self::new_inner(error.into())
    }
}

