use std::collections::HashMap;
use std::hash::Hash;
use std::borrow::Borrow;
use std::collections::hash_map::{IntoIter, Iter, Drain, IterMut};

pub struct OneToMany<K, V>(HashMap<K, Vec<V>>);

impl<K, V> OneToMany<K, V>
    where K: Hash + Eq
{
    pub fn new() -> Self {
        OneToMany(HashMap::new())
    }

    pub fn add(&mut self, key: K, value: V) {
        match self.0.get_mut(&key){
            Some(list) => list.push(value),
            None => {
                self.0.insert(key, vec![value]);
            }
        }
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn get_mut<Q: ?Sized>(&mut self, k: &Q) -> Option<&mut Vec<V>>
        where
            K: Borrow<Q>,
            Q: Hash + Eq,
    {
        self.0.get_mut(k)
    }

    pub fn get<Q: ?Sized>(&self, k: &Q) -> Option<&Vec<V>>
        where
            K: Borrow<Q>,
            Q: Hash + Eq,
    {
        self.0.get(k)
    }

    pub fn remove<Q: ?Sized>(&mut self, k: &Q) -> Option<Vec<V>>
        where
            K: Borrow<Q>,
            Q: Hash + Eq,
    {
        self.0.remove(k)
    }

    pub fn drain(&mut self) -> Drain<'_, K, Vec<V>>{
        self.0.drain()
    }

    pub fn iter(&self) -> Iter<K, Vec<V>>{
        self.into_iter()
    }

    pub fn iter_mut(&mut self) -> IterMut<K, Vec<V>>{
        self.into_iter()
    }
}

impl<'a, K, V> IntoIterator for &'a OneToMany<K, V> {
    type Item = (&'a K, &'a Vec<V>);
    type IntoIter = Iter<'a, K, Vec<V>>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<K, V> IntoIterator for OneToMany<K, V> {
    type Item = (K, Vec<V>);
    type IntoIter = IntoIter<K, Vec<V>>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<'a, K, V> IntoIterator for &'a mut OneToMany<K, V> {
    type Item = (&'a K, &'a mut Vec<V>);
    type IntoIter = IterMut<'a, K, Vec<V>>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}