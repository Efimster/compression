mod app_error;
mod file_util;
mod context;
mod context_util;

use cli_command::parse::{parse_command_line, parse_command_string};
use cli_command::cli_error::CliError;
use cli_command::command::Command;
use std::io;
use std::io::{BufRead, Read};
use app_error::AppError;
use compression::zip::enums::compression_method::CompressionMethod;
use crate::context::Context;
use compression::zip::decode_zip;
use std::path::{PathBuf, Path};
use compression::gzip::gzip;
use std::time::SystemTime;
use date::date_time::DateTime;

fn main() -> Result<(), AppError>{
    let mut context = Context::new();
    let command = parse_command_line()?;
    process_command(&command, &mut context)?;
    command_loop(&mut context);
    Ok(())
}

fn process_command(command:&Command, context:&mut Context) -> Result<(), AppError>{
    match command.name.as_ref() {
        "exit" | "q" => return Err(CliError::Exit.into()),
        "" | "help" => execute_help_command()?,
        "zip-ls" => execute_zip_ls(command, context)?,
        "zip-extract" => execute_zip_extract(command, context)?,
        "gzip-ls" => execute_gzip_ls(command, context)?,
        "gzip-extract" => execute_gzip_extract(command, context)?,
        "gzip-compress" => execute_gzip_compress(command, context)?,
        _ => {
            println!("< Unknown `{}` command", command.name);
            execute_help_command()?;
        },
    }
    Ok(())
}

fn command_loop(context:&mut Context){
    loop  {
        eprint!("> ");
        let line = io::stdin().lock().lines().next().unwrap().unwrap();
        if !parse_and_process(&line, context){
            break;
        }
    }
}

fn parse_and_process(line:&str, context:&mut Context) -> bool {
    let command = match parse_command_string(&line) {
        Ok(value) => value,
        _ => {
            println!("< Cannot parse command line");
            return true;
        },
    };
    match process_command(&command, context) {
        Err(AppError::Cli(CliError::Exit)) => return false,
        Err(err) => { println!("< Error: {:?}", err); },
        _ => (),
    }
    true
}

fn execute_help_command() -> Result<(), CliError>{
    println!("\
< zip-ls --file filepath  -->  prints archive content structure
< zip-extract --file filepath [--select <path1> <path2> ... <pathN>] [--to <directory path>]   -->  extracts zip archive
< gzip-ls --file filepath  -->  prints archive header
< gzip-extract --file filepath [--to <directory path>]   -->  extracts gzip archive
< gzip-compress --file filepath [--to <directory path>] [--comment <comment>]  -->  compress file to gzip archive

\
    ");
    Ok(())
}

fn execute_zip_ls(command: &Command, context: &mut Context) -> Result<(), AppError>{
    let cd = context_util::get_cd_headers(command, context)?;
    for (num, header) in cd.iter()
        .filter(|&header|header.uncompressed_size > 0 && header.compression_method != CompressionMethod::Store)
        .enumerate()
    {
        println!("{}. {}", num + 1, header);
    }
    Ok(())
}

fn execute_zip_extract(command: &Command, context: &mut Context) -> Result<(), AppError>{
    context_util::get_cd_headers(command, context)?;
    let cd = context.cd_headers.as_ref().unwrap();
    let mut input = context.reader.take().unwrap();
    let cd = cd.iter()
        .filter(|&header|header.uncompressed_size > 0 && header.compression_method != CompressionMethod::Store);

    let cd  = match command.get_argument("select") {
        Some(select) => {
            cd.filter(|&header|select.contains(&header.file_name)).collect::<Box<_>>()
        },
        None => cd.collect::<Box<_>>(),
    };
    let out_dir = command.get_argument_or_default("to", ".".to_string())?;
    let out = PathBuf::from(out_dir);
    for &cd_header in cd.iter() {
        print!("extracting {}.. ", &cd_header.file_name);
        let data = decode_zip::read_file(&mut input, cd_header)?;
        print!(".done {} Bytes", data.len());
        // if cd_header.marked_as_textual() {
        //     println!("{}", std::str::from_utf8(&data)?);
        // }
        let related_path = Path::new(&cd_header.file_name);
        file_util::ensure_path_exists(out.as_path(), related_path)?;
        let filepath = file_util::resolve_file_name(out.as_path(), related_path);
        print!(" -> {:?} ... ", &filepath);
        std::fs::write(filepath, data).unwrap();
        println!("done");
    }
    context.reader = Some(input);

    Ok(())
}

fn execute_gzip_ls(command: &Command, _context: &mut Context) -> Result<(), AppError>{
    let filepath = command.get_argument_nth_parameter("file", 0)
        .ok_or(AppError::Cli(CliError::MissingParameter("file".into(), 0)))?;
    let mut input = file_util::read_file(filepath)?;
    let header = gzip::gzip_ls(&mut input)?;
    println!("\t{}", header);
    Ok(())
}

fn execute_gzip_extract(command: &Command, _context: &mut Context) -> Result<(), AppError>{
    let filepath = command.get_argument_nth_parameter("file", 0)
        .ok_or(AppError::Cli(CliError::MissingParameter("file".into(), 0)))?;
    let mut input = file_util::read_file(filepath)?;
    let header = gzip::gzip_ls(&mut input)?;
    let target_file_name = if header.file_name.len() > 0 {header.file_name} else {"__gzip_no_name_extracted__".to_string()};
    print!("> Extracting {}.. ", target_file_name);
    let data = gzip::gzip_decode_compressed_data(&mut input)?;
    print!(".done");
    let out_dir = command.get_argument_or_default("to", "".to_string())?;
    if out_dir.len() == 0 {
        println!("\n{}", std::str::from_utf8(&data)?);
    }
    else {
        let out = PathBuf::from(out_dir);
        let related_path = Path::new(&target_file_name);
        file_util::ensure_path_exists(out.as_path(), related_path)?;
        let filepath = file_util::resolve_file_name(out.as_path(), related_path);
        print!(" -> {:?} ... ", &filepath);
        std::fs::write(filepath, data).unwrap();
        println!("done");
    }

    Ok(())
}

fn execute_gzip_compress(command: &Command, _context: &mut Context) -> Result<(), AppError>{
    let filepath = command.get_argument_nth_parameter("file", 0)
        .ok_or(AppError::Cli(CliError::MissingParameter("file".into(), 0)))?;
    let mut file = file_util::read_file(filepath)?;
    let seconds = file.metadata()?.modified()?.duration_since(SystemTime::UNIX_EPOCH)?.as_secs();
    let modification_date = DateTime::from_seconds_since_unix_epoch(seconds);
    let filepath = PathBuf::from(filepath);
    let input_file_name = filepath.file_name().unwrap().to_str().unwrap().to_string();
    let output_dir = command.get_argument_or_default("to", "".to_string())?;
    let file_comment = command.get_argument_or_default("comment", "".to_string())?;
    let output_dir = if output_dir.len() > 0 { Path::new(&output_dir) } else { filepath.parent().unwrap() };
    let related = PathBuf::from(format!("{}.{}", &input_file_name, "gz"));
    file_util::ensure_path_exists(output_dir, related.as_path())?;
    let output_filepath = file_util::resolve_file_name(output_dir, related.as_path());
    print!(" -> {:?} ... ", &output_filepath);
    let mut output = std::fs::File::create(output_filepath)?;
    let mut input = vec![];
    file.read_to_end(&mut input)?;
    gzip::gzip_encode(&input, Some(modification_date), input_file_name, file_comment, &mut output)?;
    println!("done");

    Ok(())
}
