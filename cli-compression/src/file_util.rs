use crate::AppError;
use cli_command::cli_error::CliError;
use std::fs;
use std::fs::File;
use std::path::Path;

pub fn read_file(filepath:&str) -> Result<File, AppError> {
    match fs::File::open(filepath){
        Ok(file) => Ok(file),
        Err(err) => Err(AppError::Cli(CliError::IO(err.kind()))),
    }
}

pub fn resolve_file_name(base:&Path, related:&Path) -> Box<Path>{
    let mut base_dir = base.to_path_buf();
    base_dir.push(related);
    if !base_dir.exists() {
        return base_dir.into_boxed_path()
    }
    let mut base_dir = base.to_path_buf();
    match related.parent() {
        Some(parent) => base_dir.push(parent),
        None => (),
    }

    let extension = match related.extension() {
        Some(extension) => extension.to_str().unwrap(),
        None => "",
    };
    let file_stem = match related.file_stem() {
        Some(stem) => stem.to_str().unwrap(),
        None => "",
    };

    let mut i = 1usize;
    loop {
        let file_name = format!("{}({}).{}", file_stem, i, extension);
        let mut filepath = base_dir.clone();
        filepath.push(file_name);
        if !filepath.exists() {
            return filepath.into_boxed_path()
        }
        i += 1;
    }
}

pub fn ensure_path_exists(base:&Path, related:&Path) -> Result<(), AppError>{
    let mut base = base.to_path_buf();
    let components = related.components().collect::<Vec<_>>();
    for i in 0 .. components.len() - 1 {
        base.push(components[i]);
        if !base.exists() {
            fs::create_dir(base.as_path())?;
        }
        else if base.is_file(){
            return Err(AppError::InvalidPath);
        }
    }
    Ok(())
}