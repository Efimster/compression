use cli_command::cli_error::CliError;
use compression::zip::enums::zip_error::ZipError;
use std::str::Utf8Error;
use std::io;
use compression::gzip::gzip_error::GZipError;
use std::time::SystemTimeError;

#[derive(Debug)]
pub enum AppError {
    Cli(CliError),
    // Panic(&'static str),
    Zip(ZipError),
    GZip(GZipError),
    IO(io::ErrorKind),
    InvalidPath,
    StringParse,
    SystemTimeError,
}

impl AppError {
    // pub fn new_panic(string:&'static str) -> AppError{
    //     AppError::Panic(string)
    // }
}

impl From<ZipError> for AppError {
    fn from(error: ZipError) -> Self {
        AppError::Zip(error)
    }
}

impl From<GZipError> for AppError {
    fn from(error: GZipError) -> Self {
        AppError::GZip(error)
    }
}

impl From<CliError> for AppError {
    fn from(error: CliError) -> Self {
        AppError::Cli(error)
    }
}

impl From<Utf8Error> for AppError {
    fn from(_: Utf8Error) -> Self {
        AppError::StringParse
    }
}

impl From<io::Error> for AppError {
    fn from(error: io::Error) -> Self {
        AppError::IO(error.kind())
    }
}

impl From<SystemTimeError> for AppError {
    fn from(_: SystemTimeError) -> Self {
        AppError::SystemTimeError
    }
}
