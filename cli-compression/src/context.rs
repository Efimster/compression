use std::io::{Read, Seek};
use compression::zip::structs::central_directory_header::CentralDirectoryHeader;

pub trait ReadNSeek : Seek + Read {}

impl<T> ReadNSeek for T where T:Read + Seek{}

pub struct Context {
    pub reader:Option<Box<dyn ReadNSeek>>,
    pub cd_headers:Option<Vec<CentralDirectoryHeader>>
}

impl Context {
    pub fn new() -> Self {
        Context {
            reader: None,
            cd_headers: None,
        }
    }
}

