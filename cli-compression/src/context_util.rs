use cli_command::command::Command;
use crate::context::{Context, ReadNSeek};
use crate::app_error::AppError;
use crate::file_util;
use cli_command::cli_error::CliError;
use compression::zip::decode_zip;
use compression::zip::structs::central_directory_header::CentralDirectoryHeader;

pub fn get_input<'a>(command: &Command, context: &'a mut Context) -> Result<&'a mut dyn ReadNSeek, AppError>{
    let filepath = command.get_argument_nth_parameter("file", 0);
    match filepath{
        Some(filepath) => {
            let input = file_util::read_file(filepath)?;
            context.reader = Some(Box::new(input));
        },
        None if matches!(context.reader, None) => return Err(AppError::Cli(CliError::MissingParameter("file".into(), 0))),
        None => (),
    }
    Ok(context.reader.as_mut().unwrap())
}

// fn execute_command(command:&str, context:&mut Context) -> Result<(), AppError> {
//     let command = parse_command_string(command)?;
//     process_command(&command, context)
// }

pub fn get_cd_headers<'a>(command: &Command, context: &'a mut Context) -> Result<&'a [CentralDirectoryHeader], AppError>{
    if let None = context.cd_headers {
        let input = get_input(command, context)?;
        let cd = decode_zip::read_central_directory_headers(input)?;
        context.cd_headers = Some(cd);
    }

    Ok(context.cd_headers.as_ref().unwrap())
}
